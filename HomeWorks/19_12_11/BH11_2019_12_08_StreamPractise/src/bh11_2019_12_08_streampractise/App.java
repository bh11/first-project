package bh11_2019_12_08_streampractise;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    static List<Country> countries = new ArrayList<>();
    
    
    public static void main(String[] args) {
        generateData();
        System.out.println("All countries:");
        printAllCountries();
        System.out.println("Distict countries:");
        printDistinctCountries();
        System.out.println("Countries where name contains \"land\"");
        printCountriesWhereNameContains("land");
        System.out.println("Count of countries where name contains \"land\"");
        printCountOfCountriesWhereNameContains("land");
        System.out.println("Countries by code \"hu\"");
        printCountryByCode("hu");
        System.out.println("Sorted countries: ");
        sortAllOfCountry();
        System.out.println("Continents: ");
        printContinents();
        System.out.println("Contry names");
        printCountryNames();
        printSummOffPopulation();
        System.out.println();
        printAllCities();
        System.out.println();
        printSummOfEuropePopulation();
    }
    
    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));
        
        Country spain = new Country("sp", "Span", "Europe", 110);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));
        
        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));
        
        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);
    }
    //basic
    public static void printAllCountries() {
        countries.forEach(p -> System.out.println(p));
    }
    //basic
    public static void printDistinctCountries() {
        countries.stream().filter(p -> p.getContinent() != "Europe").forEach(p -> System.out.println(p));
    }
    //basic
    public static void printCountriesWhereNameContains(String str) {
        countries.stream().filter(p -> p.getName().contains(str)).forEach(p -> System.out.println(p));
    }
    //basic
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(countries.stream().filter(p -> p.getName().contains(str)).count());
    }    
    //basic
    public static void printCountryByCode(String code) {
        countries.stream().filter(p -> p.getCode().equals(code)).forEach(p -> System.out.println(p));
    }
    //basic
    public static void sortAllOfCountry() {
         countries.stream().sorted((c1, c2) -> c1.getName().compareTo(c2.getName())).forEach((p) -> System.out.println(p));
    }
    //extra
    public static void printAllCities() {
         countries.stream()
                 .filter(c -> "Europe".equals(c.getContinent()))
                 .map(c -> c.getCities())
                 .flatMap(cities -> cities.stream())
                 .forEach(System.out::println);
                 
    }
    //basic
    public static void printContinents() {
        countries.stream().map((country)->country.getContinent()).distinct().forEach((c)->System.out.println(c));
    }
    //basic
    public static void printCountryNames() {
        countries.stream().distinct().forEach(p -> System.out.println(p.getName()));
    }
    //basic
    public static void printCountryCodes() {
        countries.stream().distinct().forEach(p -> System.out.println(p.getCode()));
    }    
    //extra
    public static void printSummOffPopulation() {
        countries.stream().forEach(country -> country.getCities().stream().forEach(city -> System.out.println(city.getName() + " : " + city.getPopulation())));
    }
    //extra
    public static void printSummOfEuropePopulation() {
        System.out.println("EuropePop");
       // countries.stream().filter((p -> p.getContinent().equals("Europe"))).mapToInt((country) -> country.getCities().stream().mapToInt(city -> city.getPopulation()).sum());
        int sum =        countries.stream()
                 .filter(c -> "Europe".equals(c.getContinent()))
                 .map(c -> c.getCities())
                 .flatMap(cities -> cities.stream())
                 .mapToInt(cities -> cities.getPopulation())
                        .sum();
        System.out.println(sum);
    }
    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {
        
    } 
}
