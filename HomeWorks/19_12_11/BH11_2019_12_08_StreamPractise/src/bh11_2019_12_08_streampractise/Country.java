package bh11_2019_12_08_streampractise;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private String code;
    private String name;
    private String continent;
    private int size;
    private List<City> cities;

    public Country() {
        cities = new ArrayList<>();
    }

    public Country(String code, String name, String continent, int size) {
        this();
        this.code = code;
        this.name = name;
        this.continent = continent;
        this.size = size;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
    
    public void addCity(City city) {
        this.cities.add(city);
    }

    @Override
    public String toString() {
        return "Country [name=" + name + "]";
    }
}
