/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feladat18;

import drinks.*;
import java.time.Instant;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Niki
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        Manufacturer manu1 = new Manufacturer("ManuName", "ManuAddress");
        Manufacturer manu2 = new Manufacturer("ManuNam2e", "ManuAddress2");
        
        Drink drink1 = new HungarianDrink(DrinkType.WINE, manu1 , 40, 500, true);
        Drink drink2 = new HungarianDrink(DrinkType.BEER, manu2, 34, 1500, false);
        Drink drink3 = new GermanDrink(DrinkType.WINE, manu2, 20, 2000, true, new Label(LabelColor.YELLOW, Date.from(Instant.now())));
        Drink drink4 = new FrenchDrink(DrinkType.WINE, manu2, 02, 10100, false);
        Drink drink5 = new FrenchDrink(DrinkType.PALINKA, manu1, 20, 2000, true);
        
        Store.addDrink(drink1);
        Store.addDrink(drink2);
        Store.addDrink(drink3);
        Store.addDrink(drink4);
        Store.addDrink(drink5);
        
        Store.saveDrinks();
        
        System.out.println(drink1);
        
        String input;
        String[] inputFields;
        System.out.println("CREATE típus gyártó_neve ár alkoholtartalom speciális_csoport // CREATE WINE Company 10000 5 HUNGARY");
        for (int i = 0; i < 100; i++) {
            input = scanner.nextLine();
            inputFields = input.split(" ");
            switch (inputFields[0]) {
                case "SAVE":
                    Store.saveDrinks();
                    break;
                case "CREATE":
                    Drink drink = DrinkFactory.createDrink(inputFields);
                    Store.addDrink(drink);
                    break;
            }
        }
        
        
    }
    
}
