/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feladat18;

import drinks.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Niki
 */
public class Store {

    private static final String FILE_PATH = "drinks.ser";
    private static final File drinks = new File(FILE_PATH);

    private static List<Drink> store = new ArrayList<>();

    private Store() {

    }

    public static void addDrink(Drink d) {
        store.add(d);
    }

    public static void removeDrink(Drink d) {
        store.remove(d);
    }

    public static void saveDrinks() {

        try (ObjectOutputStream ou = new ObjectOutputStream(new FileOutputStream(drinks))) {
            List<Drink> wines = store.stream().filter(d -> DrinkType.WINE.equals(d.getType())).collect(Collectors.toList());
            ou.writeObject(wines);
            System.out.println("Drinks saved!");
            report(wines);            
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    private static void report(List<Drink> wines) {
        System.out.println("All drinks: : " + wines.size());
        
        Set<Manufacturer> manufacturers = new HashSet<>();
        
        int german = 0;
        int hungarian = 0;
        int french = 0;
        int bottled = 0;
        
        for (Drink wine : wines) {
            switch (wine.getCountryOfOrigin()) {
                case FRANCE: 
                    french++;
                    break;
                case HUNGARY:
                    hungarian++;
                    break;
                case GERMANY:
                    german++;
                    break;
            }
            if (wine.isIsBottled() == true) {
                bottled++;
            }
            manufacturers.add(wine.getManufacturer());
        }
        
        System.out.println("German drinks: " + german);
        System.out.println("Hungarian drinks: " + hungarian);
        System.out.println("French drinks: " + french);
        
        System.out.println("Bottled wines: " + bottled);
        
        System.out.println("Manufacturers: ");
        
        for (Manufacturer manufacturer : manufacturers) {
            System.out.println(manufacturer);
        }
    }

}
