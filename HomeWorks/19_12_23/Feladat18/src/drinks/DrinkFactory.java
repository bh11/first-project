/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import java.time.Instant;
import java.util.Date;

/**
 *
 * @author Niki
 */
public class DrinkFactory {

    public static Drink createDrink(String[] input) {

        CountryOfOrigin country;

        try {
            country = CountryOfOrigin.valueOf(input[5]);

            switch (country) {
                case FRANCE:
                    return new FrenchDrink(DrinkType.valueOf(input[1]), new Manufacturer(input[2], ""), Double.valueOf(input[3]), Integer.valueOf(input[4]), true);
                case GERMANY:
                    return new GermanDrink(DrinkType.valueOf(input[1]), new Manufacturer(input[2], ""), Double.valueOf(input[3]), Integer.valueOf(input[4]), true, new Label(LabelColor.YELLOW, Date.from(Instant.now())));
                case HUNGARY:
                    return new HungarianDrink(DrinkType.valueOf(input[1]), new Manufacturer(input[2], ""), Double.valueOf(input[3]), Integer.valueOf(input[4]), true);
            }

        } catch (Exception e) {
            System.out.println(e);

        }
        return null;

    }

}
