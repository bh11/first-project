/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author Niki
 */
public class GermanDrink extends Drink {
    
    private Label label;

    public GermanDrink(DrinkType type, Manufacturer manufacturer, double alcohol, int price, boolean isBottled, Label label) {
        super(type, manufacturer, alcohol, price, isBottled);
        this.label = label;
        System.out.println("Drink created.");
        System.out.println(this);
    }

    

    @Override
    public CountryOfOrigin getCountryOfOrigin() {
        return CountryOfOrigin.GERMANY;
    }
    
    
    
    
}
