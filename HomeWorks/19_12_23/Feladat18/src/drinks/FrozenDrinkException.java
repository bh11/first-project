/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author Niki
 */
public class FrozenDrinkException extends Exception {

    public FrozenDrinkException(String message) {
        super(message);
    }
    
}
