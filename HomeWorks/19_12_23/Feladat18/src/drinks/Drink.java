/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import java.io.Serializable;

/**
 *
 * @author Niki
 */
public abstract class Drink implements Comparable<Drink>, Serializable {
    
    private DrinkType type;
    private Manufacturer manufacturer;
    private int eanCode;
    private double alcohol;
    private int price;
    private boolean isBottled;

    public Drink(DrinkType type, Manufacturer manufacturer, double alcohol, int price, boolean isBottled) {
        this.type = type;
        this.manufacturer = manufacturer;
        this.eanCode = generateEanCode();
        this.alcohol = alcohol;
        this.price = price;
        this.isBottled = isBottled;
    }

    public boolean isIsBottled() {
        return isBottled;
    }

    public void setIsBottled(boolean isBottled) {
        this.isBottled = isBottled;
    }

    public DrinkType getType() {
        return type;
    }

    public void setType(DrinkType type) {
        this.type = type;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getEanCode() {
        return eanCode;
    }

    public void setEanCode(int eanCode) {
        this.eanCode = eanCode;
    }

    public double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(double alcohol) {
        this.alcohol = alcohol;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    private int generateEanCode() {
        return (int)(Math.random()*99999)+1;
    
}
    public abstract CountryOfOrigin getCountryOfOrigin();

    @Override
    public String toString() {
        return "Drinks{" + "type=" + type + ", manufacturer=" + manufacturer + ", eanCode=" + eanCode + ", alcohol=" + alcohol + ", price=" + price + '}';
    }
    

    @Override
    public int compareTo(Drink o) {
        return eanCode-o.eanCode;
    }
}


