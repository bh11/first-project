/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import feladat18.Store;

/**
 *
 * @author Niki
 */
public class FrenchDrink extends Drink {
    
    private boolean isFrozen = false;

    public FrenchDrink(DrinkType type, Manufacturer manufacturer, double alcohol, int price, boolean isBottled) {
        super(type, manufacturer, alcohol, price, isBottled);
                System.out.println("Drink created.");
        System.out.println(this);
    }
    
    
    public void freeze() {
        isFrozen = true;
    }
    
    public void ship() throws FrozenDrinkException {
        if (!isFrozen) {
            System.out.println(this + " is shipped");
            Store.removeDrink(this);
        } else {
            throw new FrozenDrinkException("Couldn't ship because the drink is frozen!");
        }
    }

    @Override
    public CountryOfOrigin getCountryOfOrigin() {
        return CountryOfOrigin.FRANCE;
    }
    
}
