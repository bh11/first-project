/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import feladat18.Store;

/**
 *
 * @author Niki
 */
public class HungarianDrink extends Drink {

    public HungarianDrink(DrinkType type, Manufacturer manufacturer, double alcohol, int price, boolean isBottled) {
        super(type, manufacturer, alcohol, price, isBottled);
                System.out.println("Drink created.");
        System.out.println(this);
    }
    
    
    public void drink() {
        Store.removeDrink(this);
        System.out.println(this + " was drunk.");        
        
    }
    
    public void dilute() {
        this.setAlcohol(this.getAlcohol()*0.5);
    }

    @Override
    public CountryOfOrigin getCountryOfOrigin() {
        return CountryOfOrigin.HUNGARY;
    }
    
    
}
