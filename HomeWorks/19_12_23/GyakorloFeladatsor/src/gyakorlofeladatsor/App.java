/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyakorlofeladatsor;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 *
 * @author Lizie
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*
        1. Készíts egy generikus tároló osztályt, amelynek neve Bag<T>. 
        Alapvető tulajdonsága, hogy minden elemet többször is eltárolhat. 
        Tip: belső működésben egy ArrayList felhasználható. 
        A main metódusban próbáld ki az elkészített osztályt Stringekkel.
         */
        System.out.println("1. feladat");
        System.out.println("----------");
        Bag<String> myBag = new Bag<>();
        myBag.addElement("Alma");
        myBag.addElement("Körte");
        myBag.addElement("Barack");
        myBag.addElement("Alma");
        myBag.addElement("Meggy");

        System.out.println("The list: ");
        for (int i = 0; i < myBag.size(); i++) {
            System.out.println(myBag.getElement(i));
        }

        System.out.println("Element 1. " + myBag.getElement(1));
        System.out.println("Contains Alma? " + myBag.contains("Alma"));
        System.out.println("Contains Szilva? " + myBag.contains("Szilva"));

        System.out.println("Remove Körte");
        myBag.removeElement("Körte");

        System.out.println("The list: ");
        for (int i = 0; i < myBag.size(); i++) {
            System.out.println(myBag.getElement(i));
        }

        /*
        2. Mikor keletkezhet ArrayStoreException / ConcurrentModificationException? Mutass példát rá kóddal.
         */
        System.out.println("2. feladat");
        System.out.println("----------");
        Number[] a = new Double[2];
        try {
            a[0] = 4;
        } catch (ArrayStoreException e) {
            System.out.println("ArrayStoreException found: " + e);
        }

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);

        try {
            for (Integer number : numbers) {
                if (number == 2) {
                    numbers.remove(2);
                }
            }
        } catch (ConcurrentModificationException e) {
            System.out.println("ConcurrentModificationException found: " + e);
        }
        
        String param = "Egy paraméterből kapott stringből távolítsuk el a szóközöket.";
        String param2 = "";
        
        for (int i = 0; i <param.length(); i++) {
                if (param.charAt(i) != ' ') {
                    param2 += param.charAt(i);
            }
        }
        System.out.println(param2);

    }

}
