/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyakorlofeladatsor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Lizie
 */
public class Bag<T> {
    
    private ArrayList<T> t;
    
    public Bag() {
        t = new ArrayList<>();
    }
    
    public void addElement(T e) {
        t.add(e);
    }
    
    public List<T> getBag() {
        return Collections.unmodifiableList(t);
    }
    
    public T getElement(int index) {
        return t.get(index);
    }
    
    public void removeElement(int index) {
        t.remove(index);
    }
    
    public void removeElement(T e) {
        t.remove(e);
    }
    
    public void removeAllElement(T e) {
        t.remove(e);
    }
    
    public boolean contains(T e) {
        return t.contains(e);
    }
    
    public int size() {
        return t.size();
    }


    
  
}
