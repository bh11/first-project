/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carbuilder;

/**
 *
 * @author Niki
 */
public class Car {
     private String plate;
     private int id;
     
     private String color;

    public Car(String plate, int id) {
        this.plate = plate;
        this.id = id;
    }
     
    public Car setColor(String color) {
        this.color = color;
        return this;
    } 
}
