/*
String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
         - A fenti string hány mondatból áll? 
         - A fenti stringben hányszor szerepel az "it" részlet
         - írjuk ki a fenti szövegben az első és az utolsó "it" közötti szövegrészletet
         - írjunk ki minden második szót
         - cseréljük le az 'a' betűket 'A' betükre
         - írjuk ki fordítva (StringBuilder metódus segítségével)
Collapse
 */
package hw191124;

/**
 *
 * @author Lizie
 */
public class Hw191124 {

    /**
     * @param args the command line arguments
     */
    static String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    

    public static void main(String[] args) {
        
        str = str.trim();
        String[] words = str.split("\\s+");
        String pattern = "it";
        
        System.out.println("Number of words in string: " + words.length);
        
        System.out.println("Number of sentences in string: " + countCharacter(str, '.'));
        
        System.out.println("Occurences of \"" + pattern + "\"  in string: " + countPattern(str, pattern)[2]);
        
        System.out.println("Substring between \"" + pattern + "\"s: " + str.substring(countPattern(str, pattern)[0]+pattern.length(), countPattern(str, pattern)[1]));
        
        System.out.print("Every second word: ");
        printSecondWords(words);
        System.out.println();
        
        
        String str2 = str.replace('a', 'A');
        System.out.println("String with capital 'A's: " + str2);
        
        StringBuilder s = new StringBuilder(str);
        
        System.out.println("Reverse string: " + s.reverse().toString());
        
    }
    
    public static int countCharacter(String str, char c) {
        
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                counter++;
            }
        }
        return counter;
    }
    
    public static int[] countPattern(String str, String p) {
       
        int counter = 0;
        int firstOccurence = 0;
        int lastOccurence = 0;
        for (int i = 0; i < str.length() - p.length() + 1; i++) {
            if (str.substring(i, i + p.length()).equals(p)) {
                if (firstOccurence == 0) {
                    firstOccurence = i;
                }
                lastOccurence = i;
                counter++;
            }

            
        }
        return new int[] {firstOccurence, lastOccurence, counter};
    }
    
    static void printSecondWords(String[] words) {
        for (int i = 0; i < words.length; i=i+2) {
            System.out.print(words[i] + " ");
        }
    }
}
