/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw191204no03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Lizie
 */
public class Hw191204No03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List<String> strings = new ArrayList<>();
        strings.add("Alma");
        strings.add("Körte");
        strings.add("Mandarin");
        strings.add("Banán");
        strings.add("Narancs");
        strings.add("Szilva");
        strings.add("Dinnye");
        strings.add("Szőlő");
        
        strings.forEach((s) -> {
            System.out.print(s + " ");
        });       
        
        System.out.println("");
        System.out.println("");
        System.out.println("Ordered by length:");
        Collections.sort(strings, (p1,p2) -> p1.length() - p2.length());
        
        strings.forEach((s) -> {
            System.out.print(s + " ");
        });
        
        System.out.println("");
        System.out.println("");
        System.out.println("Reversed (by lenght):");
        Collections.sort(strings, (p1,p2) -> p2.length() - p1.length());
        
        strings.forEach((s) -> {
            System.out.print(s + " ");
        });
        
        System.out.println("");
        System.out.println("");
        System.out.println("Alphabetical:");
        Collections.sort(strings, (p1,p2) -> p1.charAt(0) - p2.charAt(0));
        
        strings.forEach((s) -> {
            System.out.print(s + " ");
        });
        
        System.out.println("");
        System.out.println("");
        System.out.println("Containing letter \'e\' first:");
        Collections.sort(strings, (p1, p2) -> p2.indexOf('e') - p1.indexOf('e'));
        
        strings.forEach((s) -> {
            System.out.print(s + " ");
        });
        
    }
    
}
