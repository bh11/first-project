/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hw191206No01and02;

/**
 *
 * @author Lizie
 */
public enum Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    
    
    public static boolean isWeekDay(Weekday day) {
        switch (day) {
            case SATURDAY:
            case SUNDAY:
                return false;
            default:
                return true;
        }        
    };
    
    public static boolean isHoliday(Weekday day) {
        return !isWeekDay(day);
    }
    
    
    
}
