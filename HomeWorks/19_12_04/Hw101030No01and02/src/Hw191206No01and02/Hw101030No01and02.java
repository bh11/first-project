/*
Create a public enum Weekday with constants for MONDAY, TUESDAY,... until SUNDAY.
The enum should have an instance method boolean isWeekDay() and an instance method boolean isHoliday().
The isHoliday() method should return the opposite of isWeekDay().
Write a program which demonstrates how this enum could be used, which has a method which takes a Weekday as the argument and prints a message depending on whether the Weekday is a holiday or not.
We suggest that the main method loops over all values in the Weekday enum and sends them as argument to the method.
Hint: every enum in Java has a static values() method, which returns an array of the values in the enum, so you may use a for-each-loop (the enhanced for loop) for this.
Hint: every enum has a toString() implementation which return the constant name as it was declared in the enum, e.g. "FRIDAY".
Expand using link to the right to see a suggested solution/answer.
 */
package Hw191206No01and02;

/**
 *
 * @author Lizie
 */
public class Hw101030No01and02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        for (Weekday day : Weekday.values()) {
            System.out.println(day + " is weekday? - " + Weekday.isWeekDay(day));
        }
        
        System.out.println("-----------------");
        
        for (Weekday day : Weekday.values()) {
            System.out.println(day + " is holiday? - " + Weekday.isHoliday(day));
        }
        
        System.out.println("-----------------");
        
        Weekday sat = Weekday.SATURDAY;
        
        for (Weekday day : Weekday.values()) {
           System.out.println(sat.compareTo(day));
        }
        
        
        
    }
}
