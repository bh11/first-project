/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw191120;

/**
 *
 * @author Lizie
 */
public class Rabbit extends Animal{
    
    private String name;

    public Rabbit(String name, int age, int countOfLegs) {
        super(age, countOfLegs);
        this.name = name;
    }
        
    @Override
    public void say() {
        System.out.println("... - It wants food or love. Usually both.");
    }

    @Override
    public String toString() {
        return name + " says: ";
    }
}
