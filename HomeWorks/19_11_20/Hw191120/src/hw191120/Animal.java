/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw191120;

/**
 *
 * @author Lizie
 */
public class Animal {

    protected int age;
    protected int countOfLegs;
    
    Animal(int age, int countOfLegs) {
        this.age = age;
        this.countOfLegs = countOfLegs;
    }

    public void say() {
        System.out.println("GRRRRR");
    }
}
