/*
Négy osztály: 
 - Animal (name:String, age:int, countOfLegs:int, method: void say()) 
 - Dog, Animal utód (color: String)
 - Cat, Animal utód (domestic: boolean)
 - Kedvenc állatod, Animal utód (légy kreatív)
 A lényeg mindhárom utódosztályban definiáld felül a say metódust, és eza metódus írja ki, hogy az adott állat milyen hangot ad ki. Mindenhol definiáld felül a toString-et!
 Készíts egy 10 elemű tömböt, töltsd fel az állatokkal, majd egy for ciklusban hívd meg a say metódust.
 */
package hw191120;

/**
 *
 * @author Lizie
 */
public class App {

    public static void main(String[] args) {
        // TODO code application logic here
        
        Animal animal1 = new Cat("Zsófi",12,4);
        System.out.println(animal1.toString());
        animal1.say();
        
        Animal animal2 = new Dog("Romeo",26,4);
        System.out.println(animal2.toString());
        animal2.say();
        
        Animal animal3 = new Rabbit("Lola",6,4);
        System.out.println(animal3.toString());
        animal3.say();
        
        
    }
    
}
