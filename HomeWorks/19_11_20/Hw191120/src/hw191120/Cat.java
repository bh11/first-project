/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw191120;

/**
 *
 * @author Lizie
 */
public class Cat extends Animal{
    
    private String name;

    public Cat(String name, int age, int countOfLegs) {
        super(age, countOfLegs);
        this.name = name;
    }
    
    
    
    @Override
    public void say() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return name + " says: ";
    }
    
    
}
