package bankinghub2;

import static bankinghub2.DebitCard.MAX_CASH_DEPOSIT_FEE;
import java.util.List;

/**
 *
 * @author Lizie
 */
public abstract class AbstractAccount {
    
    public static final int CREATE_FEE = 500;
    
    protected int moneyDepositFee = 0;
    protected String accountNumber;
    protected long balance;
    protected List<AbstractLoan> loans;
    protected List<DebitCard> debitCards;
    
    public abstract int getMonthlyFee();
    public abstract int getTrasferFee();    
    public abstract int getTakeMoneyFee();

    public int getMoneyDepositFee() {
        return moneyDepositFee;
    }

    public void setMoneyDepositFee(int moneyDepositFee) {
        this.moneyDepositFee = moneyDepositFee;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getBalance() {
        return balance;
    }
    public int getFeeOfCashDeposit(int amount) {
        return TransferMoneyUtil.getFeeOfCashDeposit(amount);
    }
    public void setBalance(long balance) {
        this.balance = balance;
    }
    
    
    
}
