/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub2;

import static bankinghub2.DebitCard.MAX_CASH_DEPOSIT_FEE;

/**
 *
 * @author Lizie
 */
public abstract class TransferMoneyUtil {
        public static int getFeeOfCashDeposit(int amount) {
        int fee = MAX_CASH_DEPOSIT_FEE;
        if (amount * 0.1 < MAX_CASH_DEPOSIT_FEE) {
            fee = (int)(amount*0.1);
        }
        return fee;
    }
}
