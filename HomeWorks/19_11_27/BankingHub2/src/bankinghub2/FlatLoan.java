/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub2;

/**
 *
 * @author Lizie
 */
public class FlatLoan extends AbstractLoan {
    
    public FlatLoan(int principle) {
        super(principle);
        this.interest = 6;
        this.tenor = 3;
        
        int interestValue = (int)(principle * (this.interest/100.0));
        
        this.installment = (int)(principle / 3.0 + interestValue / 3.0);
        
    }

    @Override
    public String toString() {
        return super.toString();
    }
    
    
    
}
