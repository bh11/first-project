/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lizie
 */
public class User {
    
    private String name;
    private int age;
    private List<AbstractAccount> accounts;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
        this.accounts = new ArrayList<>();
    }
    
    public void addAccount(AbstractAccount account) {
        this.accounts.add(account);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<AbstractAccount> getAccount() {
        return accounts;
    }

    public void setAccount(List<AbstractAccount> account) {
        this.accounts = account;
    }
    
    
    
}
