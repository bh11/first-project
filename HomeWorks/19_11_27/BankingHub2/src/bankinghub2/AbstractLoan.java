/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub2;

/**
 *
 * @author Lizie
 */
public abstract class AbstractLoan {
    
    protected int interest; // kamat
    protected int tenor; // futamidő
    protected int principal; // hitel összeg
    protected int installment;
    

    public AbstractLoan(int principal) {
        this.principal = principal;
    }

    @Override
    public String toString() {
        return "AbstractLoan{" + "interest=" + interest + ", tenor=" + tenor + ", principal=" + principal + ", installment=" + installment + '}';
    }
    
    
    
    
    
}
