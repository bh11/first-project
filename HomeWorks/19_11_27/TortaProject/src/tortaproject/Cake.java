/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tortaproject;

import java.util.ArrayList;

/**
 *
 * @author Lizie
 */
public class Cake extends AbstractSweet {
    
    protected ArrayList<String> ingredients = new ArrayList<>();
    private int maxIngredients = 5;    
    
    public Cake(String name, int calories, String[] ingredients) {
        this.fantasyName = name;
        this.calories = calories;
        int counter = 0;
        for (int i = 0; i < ingredients.length; i++) {
            if (ingredients[i] != null && !this.ingredients.contains(ingredients[i])) {
                this.ingredients.add(ingredients[i]);
                counter++;
                if (counter == maxIngredients) {
                    return;
                }
            }
            
        }
    }

    @Override
    public String servingWay() {
        return "Nem szeletelve.";
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;     
    }

    public int getMaxIngredients() {
        return maxIngredients;
    }

    public void setMaxIngredients(int maxIngredients) {
        this.maxIngredients = maxIngredients;
    }

    @Override
    public String toString() {
        return "Cake{" + "ingredients=" + ingredients + ", servingWay=" + servingWay() + '}';
    }
    
    
    
}
