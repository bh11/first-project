/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tortaproject;

/**
 *
 * @author Lizie
 */
public class BirthdayCake extends Cake {
 
    public BirthdayCake(String name, int calories, String[] ingredients) {
        super(name, calories, ingredients);
        for (int i = 5; i < ingredients.length; i++) {
            this.ingredients.add(ingredients[i]);
        }
    }
 
    
}
