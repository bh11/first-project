/*
Hf: 
Cukrászdát készítünk, négy osztály: 
 - édesség (fantázianév, kalóriaszám és egy absztrakt metódus, ami az ajánlott tálalási módot írja le, szöveggel téren vissza)
 - Torta (az édességből származtatuk, rendelkezik összetevőkkel, az összetevőkből maximum 5 db lehet és String típus)
 - Születésnapi torta (a Tortából származik, rendelkezik szülinapi kívánság szöveggel és az összetevők száma nincs korlátozva, viszont nem lehet kétszer ua. összetevő benne!)
 - Cukrászda a szimulációt tartalmazza.
 Csináljunk egy szimulációt, ahol létrehozunk 100 db tortát / szülinapi tortát random adatokkal, majd számoluk meg a benne lévő összes kalóriát + írjuk ki a szülinapi kivánságokat.
 A cukrászda tudja megmondani hogy összesen hány édességet keletkezett!
 */
package tortaproject;

import java.util.ArrayList;

/**
 *
 * @author Lizie
 */
public class Confectionary {

    public static void main(String[] args) {
       
        String[] ingr = new String[6];
        ingr[0] = "banán";
        ingr[1] = "alma";
        ingr[2] = "kifli";
        ingr[4] = "banán";
        ingr[1] = "alma";
        ingr[2] = "kifli";
        
        generateCake();
        System.out.println(getIngredient());

        Cake cake = new BirthdayCake("Torta1",50,ingr);
        
        System.out.println(cake.toString());
    }
    
    static int generateRandomNumber(int min, int max) {
        
        return (int)(Math.random()*(max-min)+min);
        
    }
    
    static Cake generateCake() {
        boolean isBirthdayCake = Math.random() < 0.5;
        Cake cake;
        if (isBirthdayCake) {
            String[] ingrediens = new String[generateRandomNumber(1,15)];
            for (int i = 0; i < ingrediens.length; i++) {
                ingrediens[i] = getIngredient();
            }
            cake = new BirthdayCake("Torta1",50,ingrediens);
        } else {
            String[] ingrediens = new String[generateRandomNumber(1,5)];
            for (int i = 0; i < ingrediens.length; i++) {
                ingrediens[i] = getIngredient();
            }
            cake = new BirthdayCake("Torta1",50,ingrediens);
        }
        
        return cake;
    }
    
    static String getIngredient() {
        
        String[] ingredients = new String[20];
        
        ingredients[0] = "egg";
        ingredients[1] = "butter";
        ingredients[2] = "milk";
        ingredients[3] = "ice cream";
        ingredients[4] = "cream cheese";
        ingredients[5] = "pumpkin";
        ingredients[6] = "ginger";
        ingredients[7] = "carrot";
        ingredients[8] = "lemon";
        ingredients[9] = "pear";
        ingredients[10] = "apricot";
        ingredients[11] = "kiwi";
        ingredients[12] = "flour";
        ingredients[13] = "cereal";
        ingredients[14] = "baking soda";
        ingredients[15] = "sponge cake";
        ingredients[16] = "wafer";
        ingredients[17] = "pancake";
        ingredients[18] = "sugar";
        ingredients[19] = "honey";
        
        return ingredients[generateRandomNumber(0, 19)];
        
    }
    
}
