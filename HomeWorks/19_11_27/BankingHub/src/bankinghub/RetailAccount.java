/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub;

/**
 *
 * @author Lizie
 */
public class RetailAccount extends AbstractAccount{
    
    protected int accountManagementFee = calcucateAccountManagementFee();
    protected double transferFeeInPercent = 0.05;
    
    public RetailAccount(Person person, int moneyToDeposit) {
        super(person, moneyToDeposit);
    }
    
    protected int calcucateAccountManagementFee() {
        if (this.balance < 250000) {
            return 1500;
        }
        else {
            return 500;
        }
    }
    
    

    @Override
    public String toString() {
        return super.toString() + "RetailAccount{" + "accountManagementFee=" + accountManagementFee + '}';
    }
    
    
    
}
