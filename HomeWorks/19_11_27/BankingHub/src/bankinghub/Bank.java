package bankinghub;

/**
 *
 * @author Lizie
 */
public class Bank {
    
    public final String NAME;
    public final int accountOpeningFee = 500;

    public Bank(String name) {
        this.NAME = name;
    }
    
    public RetailAccount openRetailAccount(Person person, int moneyToDeposit) {
        if (person.getMoney() < moneyToDeposit + 500) {
            System.out.println("Not enough money to create an account.");
            return null;
        }
        else {
            RetailAccount account = new RetailAccount(person, moneyToDeposit);
            person.decreaseMoney(accountOpeningFee);
            person.decreaseMoney(moneyToDeposit);
            return account;
        }
    }
    
}
