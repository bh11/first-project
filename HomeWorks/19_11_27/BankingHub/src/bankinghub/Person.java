/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub;

import java.util.HashMap;



/**
 *
 * @author Lizie
 */
public class Person {
    
    private final String NAME;
    private final int AGE;
    private int money;
    public HashMap<Integer, AbstractAccount> accounts;

    public Person(String name, int age, int money) {
        this.NAME = name;
        this.AGE = age;
        this.money = money;
        accounts = new HashMap<Integer, AbstractAccount>();
    }
    
    public void decreaseMoney(int amount) {
        money = money - amount;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + NAME + ", age=" + AGE + ", money=" + money + '}';
    }

    public String getName() {
        return NAME;
    }

    public int getAge() {
        return AGE;
    }

    public int getMoney() {
        return money;
    }
    
    public void listAccounts() {
        System.out.println(this);
        for (Integer i : accounts.keySet()) {
          System.out.println(accounts.get(i));
        }     
    }


    
}
