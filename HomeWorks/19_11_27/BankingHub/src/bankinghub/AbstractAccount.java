/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankinghub;

/**
 *
 * @author Lizie
 */
public abstract class AbstractAccount {
    public final int accountOpeningFee = 500;
    private static int lastAccountNumber = 1000;
    protected int balance;
    protected int accountNumber;

    public int getBalance() {
        return balance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }
    
    
    
    public static int generateAccountNumber() {
        return ++lastAccountNumber;              
    }

    public AbstractAccount(Person person, int moneyToDeposit) {
        this.balance = moneyToDeposit;
        this.accountNumber = generateAccountNumber();
    }

    @Override
    public String toString() {
        return "AbstractAccount{" + "accountOpeningFee=" + accountOpeningFee + ", lastAccountNumber=" + lastAccountNumber + ", balance=" + balance + ", accountNumber=" + accountNumber + '}';
    }
    
    
}
