package bankinghub;

/**
 *
 * @author Lizie
 */
public class DebitCard {
    
    protected final int cardNumber;
    protected final int creatingCardFee;
    protected int cashWithrawalFee;
    
    
    
    private int calculateCashWithrawalFee(int amountToWithdraw) {
        double fee = amountToWithdraw * 0.1;
        if (fee > 1000) {
            return 1000;
        }
        else {
            return (int)fee;
        }
    }
    
}
