package bankinghub;

/**
 *
 * @author Lizie
 */
public class App {

    public static void main(String[] args) {
        
        Bank bank = new Bank("BankingHub");
        
        Person person1 = new Person("Bela", 29, 100000);
        Person person2 = new Person("Anna", 17, 15000);
        Person person3 = new Person("Misi", 18, 50000);
        
        RetailAccount newAccount = bank.openRetailAccount(person3, 10000);
        
        if (newAccount != null) {
            person3.accounts.put(newAccount.getAccountNumber(), newAccount);
        }
        
        newAccount = bank.openRetailAccount(person3, 10000);
        
        if (newAccount != null) {
            person3.accounts.put(newAccount.getAccountNumber(), newAccount);
        }
        
        newAccount = bank.openRetailAccount(person3, 10000);
        
        if (newAccount != null) {
            person3.accounts.put(newAccount.getAccountNumber(), newAccount);
        }        
        
        person3.listAccounts();

    }
    
}
