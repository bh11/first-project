package hw191027no6;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191027No6 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Olvassunk be egész számokat egy 20 elemű tömbbe, majd kérjünk be egy egész számot. Keressük meg a
        tömbben az első ilyen egész számot, majd írjuk ki a tömbindexét. Ha a tömbben nincs ilyen szám, írjuk
        ki, hogy a beolvasott szám nincs a tömbben.
         */

        Scanner scanner = new Scanner(System.in);
        int[] array = new int[20];
        int number;
        boolean arrayHasNumber = false;

        System.out.println("Random számok generálása? (igen: 'i', nem: bármi más)");
        if (scanner.next().charAt(0) == 'i') {
            for (int i = 0; i < array.length; i++) {
                array[i] = (int) (Math.random() * 102 - 51);
            }
        } else {
            for (int i = 0; i < array.length; i++) {
                array[i] = readNumbers(++i + ". szám: ");
                i--;
                if (i == array.length - 1) {
                    System.out.println();
                    System.out.println("A tömb megtelt!");
                }
            }
        }

        System.out.println();
        System.out.println("A tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        number = readNumbers("Keresett szám: ");

        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                System.out.println("A tömb tartalmazza a számot, indexe: " + i);
                arrayHasNumber = true;
                break;
            }
        }

        if (!arrayHasNumber) {
            System.out.println("A beolvasott szám nincs a tömbben.");
        }
    }

    public static int readNumbers(String text) {

        Scanner scanner = new Scanner(System.in);
        boolean asking = true;

        int number = 0;

        do {
            System.out.print(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            } else {
                number = scanner.nextInt();
                {
                    asking = false;
                }
            }
        } while (asking);

        return number;
    }

}
