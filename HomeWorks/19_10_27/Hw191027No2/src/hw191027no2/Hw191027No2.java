package hw191027no2;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191027No2 {

    public static void main(String[] args) {
        /*
        FELADAT
        Egy busztársaság szeretné tudni, a napok mely óráiban hány ellenőrt érdemes terepmunkára küldenie.
        Ehhez összesítik a bírságok adatait. Egy adat a következőkből áll: „óra perc típus összeg”, ahol a típusnál
        h a helyszíni bírság, c pedig a csekk. „9 45 c 6000” jelentése: egy utas 9:45-kor csekket kapott 6 000
        forintról. A helyszíni bírságokat az ellenőrök begyűjtik; a csekkes bírságoknak átlagosan 80%-át tudják
        behajtani. (Vagyis egy 6 000-es csekk a társaság számára csak 4 800-at ér.) Az adatsor végén 0 0 x 0
        szerepel.
        Olvassa be a C programod ezeket az adatokat! Készítsen kimutatást arról, hogy mely napszakban mennyi
        a pótdíjakból a bevétel! Példakimenet:
        16:00-16:59, 14800 Ft
        17:00-17:59, 12000 Ft
         */

        int hour = 0;
        int min = 0;
        char type = 0;
        int fine = 0;
        boolean stop = false;
        int[] sum = new int[24];

        do {
            hour = readPositiveInt("Óra: ", 23);
            min = readPositiveInt("Perc: ", 59);
            do {
                type = readChar("Típus: ");
            } while (type != 'x' && type != 'c' && type != 'h');
            fine = readPositiveInt("Összeg: ", Integer.MAX_VALUE);

            if (hour == 0 && min == 0 && type == 'x' && fine == 0) {
                stop = true;
            } else {
                if (type == 'c') {
                    fine *= 0.8;
                }
                sum[hour] += fine;
            }
            System.out.println("");
        } while (!stop);

        for (int i = 0; i < sum.length; i++) {
            System.out.println(i + ":00 - " + i + ":59, " + sum[i] + " Ft");
        }
    }

    public static int readPositiveInt(String text, int max) {

        Scanner scanner = new Scanner(System.in);
        boolean asking = true;

        int number = 0;

        do {
            System.out.println(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            } else {
                number = scanner.nextInt();
                {
                    if (number >= 0 && number <= max) {
                        asking = false;
                    }
                }
            }
        } while (asking);

        return number;
    }

    public static char readChar(String text) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Típus: ");
        return scanner.next().charAt(0);
    }

}
