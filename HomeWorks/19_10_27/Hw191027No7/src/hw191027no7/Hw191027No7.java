package hw191027no7;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191027No7 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Állítsunk elő egy 50 elemű tömböt véletlen egész számokból (0-tól 9-ig terjedő számok legyenek).
        - Írjuk ki a kigenerált tömböt a képernyőre.
        - Számítsuk ki az elemek összegét és számtani középértékét.
        - Olvassunk be egy 0 és 9 közötti egész számot, majd határozzuk meg, hogy a tömbben ez a szám
        hányszor fordul elő.
         */
        
        Scanner scanner = new Scanner(System.in);
        int arrayLenght = 50;
        int[] array = new int[arrayLenght];
        int sum = 0;
        int number;
        
        for (int i = 0; i < array.length; i++) {
                array[i] = (int) (Math.random() * 10);
            }
        
        System.out.println("A tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
            sum += array[i];
        }
        
        System.out.println();
        System.out.println("Az elemek összege: " + sum);
        System.out.println("Az elemek számtani közepe: " + ((double)sum/arrayLenght));
        
        do {
            number = readNumbers("Szám (0 - 9): ");
        } while (number > 9 || number < 0);
        
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
            counter++;
            }
        }
        System.out.println("A megadott szám " + counter + "x fordul elő a tömben.");
        
    }
    
    public static int readNumbers(String text) {

        Scanner scanner = new Scanner(System.in);
        boolean asking = true;

        int number = 0;

        do {
            System.out.print(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            } else {
                number = scanner.nextInt();
                {
                    asking = false;
                }
            }
        } while (asking);

        return number;
    }
    
}
