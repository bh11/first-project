package hw191027no.pkg1;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191027No1 {

    public static void main(String[] args) {

        /*
        FELADAT:
        Írj olyan metódust, ami paraméterül kap egy stringet és egy karaktert, majd visszatér egy számmal. 
        A visszatérési érték azt jelenti, hogy az adott karakter hányszor fordul elő a tömbben.
         */
        Scanner scanner = new Scanner(System.in);

        System.out.println("Írj egy string-et: ");
        String s = scanner.nextLine();

        System.out.println("A keresett karakter: ");
        char c = scanner.next().charAt(0);

        System.out.println("\'" + c + "\' " + countCharacter(c, s) + "x fordul elő a string-ben.");
    }

    public static int countCharacter(char c, String s) {
        int count = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                count++;
            }
        }
        
        return count;
    }
}
