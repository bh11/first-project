package hw191027no5;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191027No5 {

    public static void main(String[] args) {

        /*
        FELADAT:
        Olvassunk be egész számokat 0 végjelig egy maximum 100 elemű tömbbe (a tömböt 100 eleműre
        deklaráljuk, de csak az elejéből használjunk annyi elemet, amennyit a felhasználó a nulla végjelig beír).
        - Írjuk ki a számokat a beolvasás sorrendjében.
        - Írjuk ki az elemek közül a legkisebbet és a legnagyobbat, tömbindexükkel együtt.
        - Írjuk ki az elemeket fordított sorrendben.
         */
        int countOfNumbers = 100;
        int[] numbers = new int[countOfNumbers];
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        int minIndex = 0, maxIndex = 0;

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = readNumbers(++i + ". szám: ");
            if (numbers[--i] == 0) {
                System.out.println("Kiléptél!");
                break;
            }
            if (i == numbers.length - 1) {
                System.out.println("A tömb megtelt!");
            }

            if (numbers[i] > max) {
                max = numbers[i];
                maxIndex = i;
            }
            if (numbers[i] < min) {
                min = numbers[i];
                minIndex = i;
            }
        }

        if (numbers[0] != 0) {
            System.out.println();
            System.out.print("Beírt számok: ");
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] == 0) {
                    break;
                }
                System.out.print(numbers[i] + " ");
            }
            System.out.println();

            System.out.println("Maximum: " + max);
            System.out.println("Minimum: " + min);
            System.out.print("Beírt számok visszafelé: ");
            for (int i = numbers.length - 1; i >= 0; i--) {
                if (numbers[i] != 0) {
                    System.out.print(numbers[i] + " ");
                }
            }
        }
    }

    public static int readNumbers(String text) {

        Scanner scanner = new Scanner(System.in);
        boolean asking = true;

        int number = 0;

        do {
            System.out.print(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            } else {
                number = scanner.nextInt();
                {
                    asking = false;
                }
            }
        } while (asking);

        return number;
    }

}
