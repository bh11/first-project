/*
Egy cégnél vannak Dolgozók (név: String, rendszer id: String, könyvelési egységek: Array), minden Dolgozó képes Könyveléseket 
(id: String, ország: String, számlák: Array) kezelni. Minden Könyvelési egységhez tartozhatnak Számlák (összeg: int, sorszám: String).
A dolgozókat létre tudjuk hozni Könyvelési egységek megadásával. Minden dolgozó pontosan kettő Könyvelési egységet kezel.
A Könyvelési egységeket id és ország alapján tudjuk létrehozni. A könyvelési egységek egy tömbben maximum 10 darab számlát tárolnak.
A Számla kötelező eleme a sorszám. Az összeg is megadható létrehozáskor, de ha nem adjuk meg, akkor 1000 USD értékű a számla.
A Dolgozó képes az összes hozzátartozó számla 10%-os növelésére vagy csökkentésére (ez legyen 2 külön metódus).
A dolgozó képes kilistázni a hozzátartozó számlákat (sorszám - összeg). Példányosítsuk a létrehozott osztályokat, írjuk ki a számlákat (sorszám - összeg).
Majd növeljük meg minden számla értékét. Írjuk ki az új értékeket.
 */
package hw191027;

/**
 *
 * @author Lizie
 */
public class Hw191027 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Company company = new Company();
        
        Employee emp1 = new Employee("Bela");
        Employee emp2 = new Employee("Adam");
        Employee emp3 = new Employee("Eszter");
        
        AccountingUnit acc = new AccountingUnit(0, "NÉV");


        emp1.listInvoicesOfEmployee();

        emp1.addAccountingUnit(acc);
        
        acc.addInvoice(new Invoice(2));
        acc.addInvoice(new Invoice(1, 500, "HUF"));
        acc.addInvoice(new Invoice(1, 500, "HUF"));

        
        emp1.addAccountingUnit(new AccountingUnit(1, "NÉV2"));
        emp1.addAccountingUnit(new AccountingUnit(2, "NÉV3"));
        emp1.listInvoicesOfEmployee();
        
        emp1.increaseInvoices(1.1);
        emp1.listInvoicesOfEmployee();
        emp1.decreaseInvoices(2);
        emp1.listInvoicesOfEmployee();
    }
    
}
