package hw191027;

/**
 *
 * @author Lizie
 */
public class AccountingUnit {

    private int id;
    private String country;
    private Invoice[] invoices;
    private int numberOfInvoices;

    AccountingUnit(int id, String country) {
        this.id = id;
        this.country = country;
        invoices = new Invoice[10];
    }

    public void addInvoice(Invoice invoice) {
        try {
            invoices[numberOfInvoices] = invoice;
            numberOfInvoices++;
        } catch (Exception e) {
            System.out.println("Accounting unit " + id + " already has the maximum number of invoices!");
        }
    }
    
    public Invoice[] getInvoices() {
        return invoices;
    }
    
    public void printInvoices() {
           for (int i = 0; i < numberOfInvoices; i++) {
               invoices[i].printInvoice();
        }
                        
    }

}
