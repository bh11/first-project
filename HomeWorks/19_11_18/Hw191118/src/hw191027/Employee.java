/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw191027;

/**
 *
 * @author Lizie
 */
public class Employee {
    
    	private String name;
	private AccountingUnit[] accountingUnits;
        private int numberOfAccountingUnits;

	
        Employee(String name) { 
            this.accountingUnits = new AccountingUnit[2];
            this.name = name;
        }
        
        
	public void addAccountingUnit(AccountingUnit accUnit) {
            
            try {
                accountingUnits[numberOfAccountingUnits] = accUnit;
                numberOfAccountingUnits++;
            } catch (Exception e) {
                System.out.println("Employee " + name + " already has the maximum number of accounting units!");
            }
        }
        
        public void increaseInvoices(double multiplier) {
            Invoice[] invoicesAcc0 = this.accountingUnits[0].getInvoices();
            Invoice[] invoicesAcc1 = this.accountingUnits[0].getInvoices();
            
            for (int i = 0; i < invoicesAcc0.length; i++) {
                if (invoicesAcc0[i] != null) {
                    invoicesAcc0[i].increaseInvoice(multiplier);
                }
            }
            
             for (int i = 0; i < invoicesAcc1.length; i++) {
                if (invoicesAcc1[i] != null) {
                    invoicesAcc1[i].increaseInvoice(multiplier);
                }
            }
        }
        
	public void decreaseInvoices(double divisor) {
            Invoice[] invoicesAcc0 = this.accountingUnits[0].getInvoices();
            Invoice[] invoicesAcc1 = this.accountingUnits[0].getInvoices();
            
            for (int i = 0; i < invoicesAcc0.length; i++) {
                if (invoicesAcc0[i] != null) {
                    invoicesAcc0[i].decreaseInvoice(divisor);
                }
            }
            
             for (int i = 0; i < invoicesAcc1.length; i++) {
                if (invoicesAcc1[i] != null) {
                    invoicesAcc1[i].decreaseInvoice(divisor);
                }
            }
        }        
        
	public void listInvoicesOfEmployee() {
            for (int i = 0; i < accountingUnits.length; i++) {
                if (accountingUnits[i] != null)  {
                    
                    accountingUnits[i].printInvoices();
                }       
                    
            }
        }
    
}
