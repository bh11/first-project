package hw191027;

/**
 *
 * @author Lizie
 */
public class Invoice {
    private int number;
    private double sum;
    private String currency;
    
    public final static int DEFAULT_SUM = 1000;
    public final static String DEFAULT_CURRENCY = "USD";
    
    Invoice(int number, int sum, String currency) {
        this.number = number;
        this.sum = sum;
        this.currency = currency;
    }
    
    Invoice(int number) {
        this(number, DEFAULT_SUM, DEFAULT_CURRENCY);
    }
    
    public void printInvoice() {
        System.out.println(number + " - " + sum + " " + currency);
    }
    
    public void increaseInvoice(double multiplier) {
    sum *= multiplier;
}
public void decreaseInvoice(double divisor) {
    sum /= divisor;
}
    
}
