package hw191020no8;

/**
 *
 * @author Lizie
 */

import java.util.Scanner; 

public class Hw191020No8 {
    
    public static void main(String[] args) {

        /*
        FELADAT: 
        Olvass be 2 számot. 
        Határzod meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
        X≥0 és Y≥0 → Síknegyed=1
        X≥0 és Y<0 → Síknegyed=4
        X<0 és Y≥0 → Síknegyed=2
        X<0 és Y<0 → Síknegyed=3
        */

        int x = readInt("Add meg X-et: ");
        int y = readInt("Add meg Y-t: ");
            System.out.println();
            
        if (x >= 0 && y >= 0) { 
            System.out.println("Síknegyed = 1."); 
        }
        if (x >= 0 && y < 0) { 
            System.out.println("Síknegyed = 2."); 
        }
        if (x < 0 && y >= 0) { 
            System.out.println("Síknegyed = 3."); 
        }
        if (x < 0 && y < 0) { 
            System.out.println("Síknegyed = 4."); 
        }
   
    }
    
    public static int readInt(String text) {
        
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        
        do
        {
            System.out.println(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                asking = false;
            }
        } while (asking);
        
        return scanner.nextInt();
    }
}
