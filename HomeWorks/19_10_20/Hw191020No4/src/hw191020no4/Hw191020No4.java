package hw191020no4;

/**
 *
 * @author Lizie
 */

import java.util.Scanner;

public class Hw191020No4 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Kérj be 2 számot a felhasználótól. 
        A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
        */
        
        Scanner scanner = new Scanner(System.in);
        
        boolean asking = true;
        boolean num1HasValue = false;
        int num1 = 0;
        int num2 = 0;
        
        do {
            System.out.println("Írj egy számot: ");
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                if (!num1HasValue) {
                    num1 = scanner.nextInt();
                    num1HasValue = true;
                }
                else {
                    num2 = scanner.nextInt();
                    asking = false;
                }
            }
        } while (asking);
        
        if (num1 > num2) {
            int temp = num1;
            num1 = num2;
            num2 = temp;
        }
        
        System.out.println(num1 + " és " + num2 + " közötti 11-re végződő számok: ");
        
        int counter = 0;
        for (int i = num1; i <= num2; i++)
        {
            if (Math.abs(i) % 100 == 11) {
                System.out.print(i + " ");
                counter++;
            }
        }
        if (counter == 0) {
            System.out.println("Nincs ilyen.");
        }
        
        
        
    }
}
