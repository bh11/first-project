package hw191020no9;
/**
 *
 * @author Lizie
 */

import java.util.Scanner;

public class Hw191020No9 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        A felhasználótól kérj be 2 egész számot 10 és 500 között. 
        Határozd meg a két pozitív egész szám legnagyobb közös osztóját!
        */
        
        Scanner scanner = new Scanner(System.in);
        
        boolean asking = true;
        int num;
        int num1 = 0;
        int num2 = 0;
        
        do {
            if (num1 == 0) {
                System.out.println("Írj egy számot 10 és 500 között: ");
            }
            else {
                System.out.println("Írj még egy számot 10 és 500 között: ");
            }
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                num = scanner.nextInt();
                if (num >= 10 && num <= 500) {
                    if (num1 == 0) {
                        num1 = num;
                    }
                    else {
                        if (num > num1) {
                            num2 = num1;
                            num1 = num;
                        }
                        else {
                            num2 = num;
                        }
                        asking = false;
                    }
                }
            }
                
        } while (asking);
        
        int temp;
        while (num2 > 0) {
            temp = num2;
            num2 = num1 % num2;
            num1 = temp;
        }
        System.out.println("A két szám legnagyobb közös osztója: " + num1);
    }
}