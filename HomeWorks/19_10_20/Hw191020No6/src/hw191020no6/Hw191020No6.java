package hw191020no6;

public class Hw191020No6 {

    public static void main(String[] args) {

        /*
        FELADAT:
        Írd ki a páros számokat 55 és 88 között. 
        Oldd meg ezt a feladatot while és for felhasználásával is.
        */
        
        int num1 = 55, num2 = 88;
        
        System.out.println("Páros számok " + num1 + " és " + num2 + " között: ");
        
        for (int i = num1; i <= num2; i++)
        {
            if (i % 2 == 0)
            {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        
        int j = num1;
        while (j <= num2)
        {
            if (j % 2 == 0)
            {
                System.out.print(j + " ");
            }
            j++;
        }
        
    }
    
}
