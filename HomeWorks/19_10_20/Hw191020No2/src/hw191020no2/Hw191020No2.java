package hw191020no2;

/**
 *
 * @author Lizie
 */
public class Hw191020No2 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Sorsolj ki egy egész számot az [1000;10000] intervallumból. 
        Írd ki az első és utolsó számjegyét!
        */
        
        int max = 10000;
        int min = 1000;
        
        int num = (int)(Math.random() * ((max - min)+1) + min);
        int digits = String.valueOf(num).length();
        
        System.out.println("Random szám: " + num);
        
        System.out.println("Első számjegy: " + (int)(num / (Math.pow(10, digits -1))));
        System.out.println("Utolsó számjegy: " + num % 10);
        
        
    }
    
}
