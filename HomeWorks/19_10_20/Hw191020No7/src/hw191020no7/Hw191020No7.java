package hw191020no7;

import java.util.Scanner;

public class Hw191020No7 {

    public static void main(String[] args) {

        /*
        FELADAT:
        Kérj be egy számot a felhasználótól. 
        Ha 7-tel osztható, írd ki, hogy "kismalac". 
        Ezt a feladatot oldd meg if és switch felhasználásával is.
        */
        
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        boolean asking = true;
             
        do {
            System.out.println("Írj egy számot: ");
            if (!scanner.hasNextInt())
            {
                scanner.next();
            }
            else
            {
                num = scanner.nextInt();
                asking = false;
            }
                
        } while (asking);
        
        if (num % 7 == 0) {
            System.out.println("Kismalac!");
        }
        else {
            System.out.println("Nem kismalac.");
        }
            
        switch (num % 7) {
            case 0:
                System.out.println("Kismalac!");
                break;
            default:
                System.out.println("Nem kismalac.");
        }
        
    }
    
}
