package hw191020no3;

import java.util.Scanner;

public class Hw191020No3 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Kérj be egy pozitív egész számot (addig kérjünk be adatot, amíg pozitív számot nem kapunk). 
        Írd ki 0-tól az adott számig a számog négyzetét. Pl.: bemenet: 4 - kimenet: 0 1 4 9 16
        */
        
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        boolean asking = true;
             
        do {
            System.out.println("Írj egy pozitív számot: ");
            if (!scanner.hasNextInt())
            {
                scanner.next();
            }
            else
            {
                num = scanner.nextInt();
                if (num > 0)
                {
                    asking = false;
                }
            }
                
        } while (asking);
        
        for (int i = 0; i <= num; i++)
        {
            System.out.print(i * i + " ");
        }        
    }    
}
