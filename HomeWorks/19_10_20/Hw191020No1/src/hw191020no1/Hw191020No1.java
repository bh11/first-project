package hw191020no1;

import java.util.Scanner;

public class Hw191020No1 {


    public static void main(String[] args) {
        
        /*
        FELADAT:
        Kérj be egy számot, írd ki a páros osztóit!
        */
        
        Scanner scanner = new Scanner(System.in);
        int num = 0, counter = 0;
        boolean asking = true;
             
        do {
            System.out.println("Írj egy pozitív számot: ");
            if (!scanner.hasNextInt())
            {
                scanner.next();
            }
            else
            {
                num = scanner.nextInt();
                if (num > 0)
                {
                    asking = false;
                }
            }
                
        } while (asking);
        
        System.out.println("A szám páros osztói: ");
        
        for (int i = 1; i <= num; i++) {
            if (num % i == 0)
            {
                if (i % 2 == 0)
                {
                    System.out.print(i + " ");
                    counter++;
                }
            }
        }
        
        if (counter == 0) {
            System.out.println("A számnak nincs páros osztója.");
        }
        
    }
    
}
