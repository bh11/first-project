package hw191020no5;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191020No5 {

    public static void main(String[] args) {
        
        /*
        FELADAT:
        Írj programot, mely addig olvas be számokat a billentyűzetről, ameddig azok kisebbek, mint tíz. 
        Írd ki ezek után a beolvasott számok összegét!
        */
        
        int num;
        int sum = 0;
        
        System.out.println("Írj 10-nél kisebb számokat.");
        
        do {
            num = readInt();
            if (num < 10) {
                sum += num;
            }
        } while (num < 10);
        
        System.out.println("A számok összege: " + sum);
    }
    
    public static int readInt() {
        
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        
        do
        {
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                asking = false;
            }
        } while (asking);
        
        return scanner.nextInt();
    }
}
