package hw191025no1;

/**
 *
 * @author Lizie
 */
public class Hw191025No1 {

    public static void main(String[] args) {

        /*
        FELADAT:
        tomb, 50 elem, [-100, 100] értékkészlet, írjuk ki a következőket: 
        - maximum hely és érték,
        - minimum hely és érték, 
        - átlag 
        - összeg
         */
        
        int[] array = new int[50];
        int sum = 0;
        int max, min;
        int max_index = 0, min_index = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 201) - 100;
        }
        
        max = array[0];
        min = array[0];
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            if (array[i] > max) {
                max = array[i];
                max_index = i;
            }
            if (array[i] < min) {
                min = array[i];
                min_index = i;
            }
        }
        System.out.println("A tömb elemei: ");
        
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        
        System.out.println("");
        System.out.println("Maximum: " + max + " Hely: " + max_index);
        System.out.println("Minimum: " + min + " Hely: " + min_index);
        System.out.println("Összeg: " + sum);
        System.out.println("Átlag: " + (sum / array.length));

    }

}
