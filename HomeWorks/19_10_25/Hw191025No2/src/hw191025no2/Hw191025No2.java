package hw191025no2;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191025No2 {

    public static void main(String[] args) {

        /*
        FELADAT:
        kérjük be egy pozitív egész számot (ellenőrzés), majd írjunk ki csillagokat az alábbi formában a megadott szám alapján: 
        a. 
        szam = 7
        *******
        ******
        *****
        ****
        ***
        **
        *
        b. 
        szam = 5 (ha páros a szám, vonjunk ki belőle egyet)
          *
         ***
        *****
        */
        
        int num = readPositiveInt("Adj meg egy pozitív egész számot: ");

        System.out.println("A: ");
        
        for (int i = num; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.print("\n");
            
        }
        
        System.out.println("B: ");
        
        if (num % 2 == 0) {
            num--;
        }
        
        int stars = 1;
        
        while (stars <= num)
        {
            for (int i = 0; i < (num - stars) / 2; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < stars; i++) {
                System.out.print("*");
            }
            for (int i = 0; i < (num - stars) / 2; i++) {
                System.out.print(" ");
            }
            stars +=2;
            System.out.println("");
            
        }
        
    }
    public static int readPositiveInt(String text) {
        
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        
        int number = 0;
        
        do
        {
            System.out.println(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                number = scanner.nextInt();
                {
                    if (number > 0) {
                        asking = false;
                    }
                }
            }
        } while (asking);
        
        return number;
    }
    
}
