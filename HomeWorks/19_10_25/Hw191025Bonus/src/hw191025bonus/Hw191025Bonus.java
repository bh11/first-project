package hw191025bonus;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw191025Bonus {

    public static void main(String[] args) {
        /*
        FELADAT
        bekérünk egy felhasználótol egy számot, váltsuk át kettes számrendszerbe
        */
        
        int num = readInt("Írj egy számot: ");
        ArrayList<Integer> list = new ArrayList<Integer>();
        
        while (num > 0) {
            list.add(num % 2);
            num /= 2;
        }
        
        System.out.println("Kettes számrendszerben: ");

        for (int i = list.size()-1; i >= 0 ; i--) {
            System.out.print(list.get(i));
        }
        System.out.println("");
        
    }
    
    public static int readInt(String text) {
        
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        
        do
        {
            System.out.println(text);
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            else {
                asking = false;
            }
        } while (asking);
        
        return scanner.nextInt();
    }
    
}
