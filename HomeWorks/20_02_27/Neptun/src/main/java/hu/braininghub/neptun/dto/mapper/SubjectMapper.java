/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.dto.mapper;

import hu.braininghub.neptun.dto.SubjectDto;
import hu.braininghub.neptun.repository.entity.Subject;
import java.util.ArrayList;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Niki
 */
@Singleton
public class SubjectMapper implements Mapper<Subject, SubjectDto> {

    @Inject
    private StudentMapper studentMapper;

    @Override
    public Subject toEntity(SubjectDto dto) {

        Subject s = new Subject();

        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setDescription(dto.getDescription());

        return s;
    }

    @Override
    public SubjectDto toDto(Subject entity) {

        SubjectDto s = new SubjectDto();

        s.setId(entity.getId());
        s.setName(entity.getName());
        s.setDescription(entity.getDescription());

        return s;
    }

}
