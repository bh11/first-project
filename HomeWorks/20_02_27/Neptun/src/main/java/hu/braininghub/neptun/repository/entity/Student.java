/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.repository.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Niki
 */

@Data
@Entity
@Table(name = "STUDENT")
public class Student implements Serializable {
    
    @Id
    @Column(name = "student_id")
    private Integer id;
    
    @Column(name = "student_name")
    private String name;
    
    @Column(name = "neptun_code")
    private String neptunCode;
    
    @Column(name = "date_of_birth")
    private Date dateOfBirth;
    
    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER, cascade = CascadeType.ALL)            
    private List<Subject> subjects;
    
            
}
