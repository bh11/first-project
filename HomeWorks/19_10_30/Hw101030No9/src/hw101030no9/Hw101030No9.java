/*
FELADAT:
Írj egy függvényt, ami a paraméterként átvett kis betűnek megfelelő nagybetűt adja vissza!
 */
package hw101030no9;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw101030No9 {

    static Scanner SCANNER = new Scanner(System.in, "ISO-8859-1");

    static char getCharacter(char c) {
        return Character.toUpperCase(c);
    }

    public static void main(String[] args) {
        System.out.println(getCharacter(SCANNER.next().charAt(0)));
    }
}
