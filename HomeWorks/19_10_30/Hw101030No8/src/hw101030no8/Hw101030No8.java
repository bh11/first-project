/*
FELADAT:
Írj függvényt, ami egy tömböt átvesz paraméterként és hátulról indulva kiírja minden második elemét!
Ügyelj arra, hogy nehogy túl/alulindexeld a tömböt!
 */
package hw101030no8;

/**
 *
 * @author Lizie
 */
public class Hw101030No8 {

    static void printArrayBackwards(int[] array) {
        int counter = 1;
        for (int i = array.length - 1; i >= 0; i--) {
            if (counter % 2 == 0) {
                System.out.print(array[i] + " ");
            }
            counter++;
        }
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        printArrayBackwards(array);
    }
}
