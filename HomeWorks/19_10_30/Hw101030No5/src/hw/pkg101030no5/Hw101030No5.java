/*
FELADAT:
Írj függvényt, amely a paramétereként átvett pozitív, egész számról eldönti, hogy az prím-e! Logikai
visszatérése legyen, amely akkor IGAZ, ha a paraméterként átvett szám prím, különben HAMIS.
Írj programot, amely a felhasználótól bekér egy számot, és a fenti függvény segítségével kiírja a
képernyőre az annyiadik prímszámot.
 */
package hw.pkg101030no5;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw101030No5 {

    static Scanner SCANNER = new Scanner(System.in);

    static int getPositiveNumber(String message) {
        do {
            System.out.println(message);
            if (!SCANNER.hasNextInt()) {
                SCANNER.next();
            } else {
                int number = SCANNER.nextInt();
                if (number > 0) {
                    return number;
                }
            }
        } while (true);
    }

    static boolean isPrime(int number) {
        for (int i = 2; i <= number / 2; ++i) {
            if (number % i == 0) {
                return false;
            }
        }
        return (number > 1) ? true : false;
    }

    static int getNthPrime(int nth) {
        int counter = 0;
        int num = 1;

        while (counter < nth) {
            if (isPrime(++num)) {
                counter++;
            }
        }
        return num;
    }

    public static void main(String[] args) {

        int nth = getPositiveNumber("Give me a positive number: ");
        System.out.printf("The %d. prime number: %d ", nth, getNthPrime(nth));
    }
}
