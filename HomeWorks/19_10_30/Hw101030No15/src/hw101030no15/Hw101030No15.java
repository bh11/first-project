/*
FELADAT:
Készítsünk olyan függvényt, amely meghatározza egy tömbben hány 3-al osztható, de 5-el nem osztható
szám van!
 */
package hw101030no15;

import java.util.ArrayList;

/**
 *
 * @author Lizie
 */
public class Hw101030No15 {
    
    static int MIN = 0;
    static int MAX = 50;

    static int generateRandomInt(int min, int max) {
        return (int)(Math.random() * ((max - min)+1) + min);
    }
    
    static void fillUpArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = generateRandomInt(MIN,MAX);
        }
    }
   
    static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
    
    static int[] divisibleBy3NotBy5(int[] array) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 3 == 0 && array[i] % 5 != 0)
                numbers.add(array[i]);
        }
        return convertArraylistToArray(numbers);
    }
    
    static int[] convertArraylistToArray(ArrayList list) {
        int[] tempArray = new int[list.size()];
        for (int i=0; i < tempArray.length; i++)
        {
            tempArray[i] = (int)list.get(i);
        }
        return tempArray;
    }
    
    
    
    public static void main(String[] args) {
        
        int[] array = new int[10];
        fillUpArray(array);
        System.out.println("A tömb elemei: ");
        printArray(array);
        int[] divisibleNumbers = divisibleBy3NotBy5(array);
        System.out.printf("A tömbben %d hárommal oszható, de öttel nem osztható elem van: ", divisibleNumbers.length);
        printArray(divisibleNumbers);
        
    }
    
}
