/*
FELADAT:
Készítsünk olyan függvényt, amely egy téglalap két oldalának ismeretében kiszámítja a téglalap területét!
Az eredmény legyen a visszatérési érték. Ehhez írjunk programot, amiben ki is próbáljuk a függvényt.
 */
package hw101030no14;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw101030No14 {

    static Scanner SCANNER = new Scanner(System.in);

    static int getPositiveNumber(String message) {
        do {
            System.out.println(message);
            if (!SCANNER.hasNextInt()) {
                SCANNER.next();
            } else {
                int number = SCANNER.nextInt();
                if (number > 0) {
                    return number;
                }
            }
        } while (true);
    }

    static int calculateArea(int a, int b) {
        return a * b;
    }

    public static void main(String[] args) {
        System.out.printf("A téglalap területe: %d ", calculateArea(getPositiveNumber("A téglalap egyik oldala: "), getPositiveNumber("A téglalap másik oldala: ")));

    }

}
