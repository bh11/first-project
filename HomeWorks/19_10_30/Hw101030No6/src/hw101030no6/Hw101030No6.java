/*
FELADAT:
Írj függvényt, amely paraméterként átvesz két egész számot és visszaadja azt, amelyiknek az abszolút
értéke nagyobb!
 */
package hw101030no6;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Hw101030No6 {

    static Scanner SCANNER = new Scanner(System.in);

    static int getNumber(String message) {
        do {
            System.out.println(message);
            if (!SCANNER.hasNextInt()) {
                SCANNER.next();
            } else {
                return SCANNER.nextInt();
            }
        } while (true);
    }
    
    static int getTheLargerNumber(int num1, int num2) {
        return (num1 > num2) ? Math.abs(num1) : Math.abs(num2);
    }

    public static void main(String[] args) {
        System.out.printf("A nagyobb abszolútértékű szám: %d ", getTheLargerNumber(getNumber("Első szám: "), getNumber("Második szám: ")));
    }

}
