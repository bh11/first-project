package pkg191030gyak01;

import java.util.Scanner;

/*
        FELADAT:
        Felhasználó megad 2 egész számot 10 és 15 között.
        Ebből generálunk egy 2D-s pályát.
        Bombák száma: 8-20 között (felhasználó adja meg)
        2 típusú bommba, 70% azonnali halál, 30% -1 élet
        Kezdeti életek száma: 5
        Játékos csak jobbra és lefelé tud lépni (kezdet: 0,0)
        Cél: utolsó sorba eljutni úgy hogy nem halunk meg: SOUT(hány élet maradt, hány lépésből oldottuk meg)
 */
/**
 *
 * @author Niki
 */
public class Main {

    static final Scanner SCANNER = new Scanner(System.in);

    static final char GOOD_BOMB = 'D';
    static final char BAD_BOMB = '*';
    static final char PLAYER = '8';
    static final char GIFT = 'G';

    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;

    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    static final int NUMBER_OF_GIFTS = 3;

    static final char EMPTY = '\0';

    static int lifePoints = 5;
    static int playerX = 0;
    static int playerY = 0;

    static int getNumber(int from, int to) {
        do {

            System.out.printf("Give me a number between %d and %d. ", from, to);

            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();

                if (number >= from && number <= to) {
                    return number;
                }

            } else {
                swallowString();

            }

        } while (true);

    }

    static void swallowString() {
        SCANNER.next();
    }

    static void printField(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print(" ");
                } else {
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
    }

    static void play(int row, int col) {
        char[][] field = new char[row][col];

        putPlayer(field);
        printField(field);
        putBombs(field);

        step(field);

    }

    static void putPlayer(char[][] field) {
        field[playerX][playerY] = PLAYER;
    }
    
    static void removePlayer(char[][] field) {
        field[playerX][playerY] = EMPTY;
    }

    static void putBombs(char[][] field) {
        int numberOfBombs = getNumber(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
        int numberOfGoodBombs = calculateNumberOfGoodBombs(numberOfBombs);
        int numberOfBadBombs = numberOfBombs - numberOfGoodBombs;

        setSpecialTiles(field, numberOfGoodBombs, GOOD_BOMB);
        setSpecialTiles(field, numberOfBadBombs, BAD_BOMB);
        setSpecialTiles(field, NUMBER_OF_GIFTS, GIFT);

        printField(field);

    }

    static int calculateNumberOfGoodBombs(int numberOfBombs) {
        return numberOfBombs / 3;
    }

    /**
     * This method returns a random number between from and to excluding to
     *
     * @param from
     * @param to
     * @return
     */
    static int generateRandomNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void setSpecialTiles(char[][] field, int count, char character) {
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[i].length);

            if (field[x][y] == EMPTY) {
                field[x][y] = character;

            } else {
                i--;

            }

        }

    }

    static void step(char[][] field) {
        while (lifePoints > 0 && !isPlayerInTheLastRow(field)) {
            char c = SCANNER.next().charAt(0);

            switch (c) {
                case 'r':
                    stepRight(field);
                    break;
                case 'd':
                    stepDown(field);
                    break;
                case 'l':
                    stepLeft(field);
                    break;
                case 'u':
                    stepUp(field);
                    break;
            }

            printField(field);
        }
    }

    static void stepLeft(char[][] field) {
        if (!isPlayerInTheFirstColumn(field)) {
            handleSpecialTiles(field, playerX, playerY + 1);
            movePlayer(field, 'l');

        }

    }

    static void stepUp(char[][] field) {
        if (!isPlayerInTheFirstRow(field)) {
            handleSpecialTiles(field, playerX + 1, playerY);

            movePlayer(field, 'u');

        }
    }

    static void stepRight(char[][] field) {
        if (!isPlayerInTheLastColumn(field)) {
            handleSpecialTiles(field, playerX, playerY + 1);

            movePlayer(field, 'r');

        }

    }

    static void stepDown(char[][] field) {
        if (!isPlayerInTheLastRow(field)) {
            handleSpecialTiles(field, playerX + 1, playerY);

            movePlayer(field, 'd');
        }
    }

    static void movePlayer(char[][] field, char direction) {
        if (lifePoints > 0) {
            removePlayer(field);
            switch (direction) {
                case 'd':
                    playerX++;
                    break;
                case 'r':
                    playerY++;
                    break;
                case 'u':
                    playerX--;
                    break;
                case 'l':
                    playerY--;
                    break;
            }
            putPlayer(field);

        }
    }

    static void handleSpecialTiles(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMB) {
            lifePoints--;
        }

        if (field[x][y] == BAD_BOMB) {
            lifePoints = 0;
        }

        if (field[x][y] == GIFT) {
            lifePoints++;
        }
    }

    static boolean isPlayerInTheLastColumn(char[][] field) {
        return playerY == field[0].length - 1;
    }

    static boolean isPlayerInTheFirstColumn(char[][] field) {
        return playerY == 0;
    }

    static boolean isPlayerInTheLastRow(char[][] field) {
        return playerX == field.length - 1;
    }

    static boolean isPlayerInTheFirstRow(char[][] field) {
        return playerX == 0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);

        play(row, col);

    }

}
