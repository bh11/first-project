package hu.braininghub.hrproject;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Niki
 */
public class App {

    private static final String STOP = "EXIT";

    public static void main(String[] args) {

        HrDao dao = new HrDao();
   //    SwingView view = new SwingView();

        try (Scanner sc = new Scanner(System.in)) {
            String line;
            do {
                line = sc.nextLine();
                switch (line) {
                    case "1":
                        dao.findEmployeeWithMaxSalary();
                        break;
                    case "2":
                       dao.findDepartmentWithMostEmployees();
                        break;
                    case "3":
                        break;
                    case "4":
                        break;
                    case "5":
                        break;
                    case "6":
                        System.out.println("Give me an ID: ");
         
                        dao.findEmployeeById(sc.nextLine());
                        break;
                    case "7":
                                                System.out.println("Give me a name: ");

                        dao.findEmployeeByFirstName(sc.nextLine());
                        break;

                }
            } while (!STOP.equalsIgnoreCase(line));
        }
    }
}
