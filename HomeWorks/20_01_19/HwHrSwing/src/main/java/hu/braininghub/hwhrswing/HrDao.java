package hu.braininghub.hrproject;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Niki
 */
public class HrDao {
    
    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "root";
    
    
    public void findEmployeeWithMaxSalary() {
        String sql = "SELECT * FROM employees WHERE salary = (SELECT MAX(salary) FROM employees)";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            
            ResultSet rs = stm.executeQuery(sql);
            
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void findDepartmentWithMostEmployees() {
        String sql = "SELECT departments.department_name, COUNT(employees.employee_id) AS employees_in_department FROM employees INNER JOIN departments ON employees.department_id = departments.department_id GROUP BY employees.department_id ORDER BY employees_in_department DESC LIMIT 1;";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            
            ResultSet rs = stm.executeQuery(sql);
            
            while (rs.next()) {
                System.out.println(rs.getString("department_name"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void findEmployeeById(String id) {
        String sql = "SELECT * FROM employees WHERE employee_id = '" + id +"'";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            
            ResultSet rs = stm.executeQuery(sql);
            
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
     public void findEmployeeByFirstName(String name) {
        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.first_name = ?";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
