package hu.braininghub.hwhrswing;


import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Niki
 */
public class SwingView extends JFrame {

    JPanel panel1;
    JPanel panel2;
    JTextArea textArea;
    JScrollPane scrollPane;
    JButton button1;
    JButton button2;
    JButton button3;
    JButton button4;
    JButton button5;

    public SwingView() {
        init();
    }

    private void init() {
        setUpWindow();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("HrProject");

        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());

        textArea = new JTextArea(20, 20);
        textArea.setEditable(false);
        scrollPane = new JScrollPane(textArea);

        panel1.add(scrollPane, BorderLayout.CENTER);
        panel1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        panel2 = new JPanel();

        button1 = new JButton("CLICK");
        button2 = new JButton("CLICK");
   //     button.addActionListener(l -> c.handleButtonClick());
        panel2.add(button1);
        panel2.add(button2);
        panel2.add(button3);
        panel2.add(button4);
        panel2.add(button5);

        panel1.add(panel2, BorderLayout.NORTH);
        this.add(panel1);
    }

    private void showWindow() {
        this.setVisible(true);
    }

}
