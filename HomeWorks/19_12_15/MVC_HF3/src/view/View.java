package view;

import controller.Controller;
import java.util.List;
import model.Employee;

/**
 *
 * @author Lizie
 */
public interface View {
    
    public void SetController(Controller c);

    public void StartView();
    
    public void printResult(List<Employee> employees);
    
}
