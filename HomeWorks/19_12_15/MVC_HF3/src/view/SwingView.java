package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import model.Employee;
import model.Worker;

/**
 *
 * @author Lizie
 */
public class SwingView implements View {

    private Controller c;
    private String name;

    JFrame f = new JFrame("SwingView");
    JPanel p = new JPanel();
    JButton newWorkerButton = new JButton("Add Worker");
    JButton newBossButton = new JButton("Add Boss");
    JButton deleteButton = new JButton("Delete All");
    JTextArea textArea = new JTextArea(15, 15);

    @Override
    public void SetController(Controller c) {
        this.c = c;
    }

    @Override
    public void StartView() {
        
        c.notifyView();

        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                if (c.saveEmployeesOnExit()) {
                    System.out.println("Employees saved.");
                }
                f.dispose();
                System.exit(0);
            }
        });
        
        f.add(p);
        
        p.setLayout(new BorderLayout());

        p.add(textArea, BorderLayout.SOUTH);
        p.add(newWorkerButton, BorderLayout.WEST);
        p.add(newBossButton, BorderLayout.CENTER);
        p.add(deleteButton, BorderLayout.EAST);

        f.setSize(300, 300);
        f.setVisible(true);

        newWorkerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                name = JOptionPane.showInputDialog("Enter name");
                if (name != null && !name.equals("")) {
                    c.addEmployee(c.newWorker(name));

                }
            }
        });

        newBossButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                name = JOptionPane.showInputDialog("Enter name");
                if (name != null && !name.equals("")) {
                    c.addEmployee(c.newBoss(name));

                }
            }
        });
        
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c.deleteEmployees();
            }
        });

    }

    @Override
    public void printResult(List<Employee> employees) {
        String empList = "";

        for (Employee employee : employees) {
            empList += employee + "\n";
        }
        
        textArea.setText(empList);
    }

}
