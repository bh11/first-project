package view;

import controller.Controller;
import java.util.List;
import java.util.Scanner;
import model.Employee;

/**
 *
 * @author Lizie
 */
public class ConsoleView implements View {

    private Controller c;
    private final String STOP = "exit";
    private final Scanner sc = new Scanner(System.in);

    @Override
    public void StartView() {
        System.out.println("Commands: \n list - List employees \n delete - Delete all employees \n worker \'name\' - Add worker \n boss \'name\' - Add boss \n exit - Exit");

        String[] words;
        do {
            words = sc.nextLine().split("\\s+");
            switch (words[0]) {
                case "list":
                    c.notifyView();
                    break;
                case "delete":
                    c.deleteEmployees();
                    break;
                case "worker":
                    if (words.length > 1 && !words[1].equals("")) {
                        c.addEmployee(c.newWorker(words[1]));
                    }
                    break;
                case "boss":
                    if (words.length > 1 && !words[1].equals("")) {
                        c.addEmployee(c.newBoss(words[1]));
                    }
                    break;
            }
        } while (!STOP.equals(words[0]));
        if (c.saveEmployeesOnExit()) {
            System.out.println("Employees saved.");
        }
    }

    @Override
    public void SetController(Controller c) {
        this.c = c;
    }

    @Override
    public void printResult(List<Employee> employees) {
        if (employees != null) {
            for (Employee employee : employees) {
                System.out.println(employee);
            }
        }
        else {
            System.out.println("No employees.");
    } 
}
}
