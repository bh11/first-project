package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Lizie
 */
public class EmployeeStore {
    
    private static List<Employee> employees;
    private static final String FILE_PATH = "employees.ser";
    private static final File file = new File(FILE_PATH);
    
    private EmployeeStore() {
        
    }
    
    public static List<Employee> getEmployees() {
        if (employees == null) {
            init();
        }
        return employees;
    }
    
    private static void init() {
        try (ObjectInputStream oi = new ObjectInputStream(new FileInputStream(file))) {
            employees = (ArrayList<Employee>) oi.readObject();
            if (employees == null) {
                employees = new ArrayList<>();
            }
        } catch (IOException ex) {
            employees = new ArrayList<>();
        } catch (ClassNotFoundException ex) {
            System.out.println("Something is wrong.");
        }
    }
    
    public static void saveEmployees() throws IOException {
        try (ObjectOutputStream ou = new ObjectOutputStream(new FileOutputStream(file))) {
            ou.writeObject(employees);
        } 
    }
    
    public static void deleteEmployees() {
        employees.removeAll(employees);
    }
    
    
    
}
