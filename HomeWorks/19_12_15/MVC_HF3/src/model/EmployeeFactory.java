package model;

/**
 *
 * @author Lizie
 */
public class EmployeeFactory {
    
    public static Worker createWorker(String name) {
        return new Worker(name);
    }
    
    public static Boss createBoss(String name) {
        return new Boss(name);
    }
    
}
