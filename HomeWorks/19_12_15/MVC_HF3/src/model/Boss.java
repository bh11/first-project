package model;

/**
 *
 * @author Lizie
 */
public class Boss extends Employee {
    
    public Boss(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Boss{" + super.getName() + '}';
    }
    
    
    
}
