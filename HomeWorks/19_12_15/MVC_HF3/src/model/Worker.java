package model;

/**
 *
 * @author Lizie
 */
public class Worker extends Employee {
    
    public Worker(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Worker{" + super.getName() + '}';
    }
    
    
    
}
