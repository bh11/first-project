package model;

import java.io.Serializable;

/**
 *
 * @author Lizie
 */
public abstract class Employee implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + '}';
    }
    
    
    
}
