package controller;

import java.io.IOException;
import java.util.List;
import model.*;
import view.View;

/**
 *
 * @author Lizie
 */
public class Controller {
    
    private Model m;
    private View v;
    
    public Controller(Model m, View v) {
        this.m = m;
        this.v = v;
        
        m.setController(this);
        v.SetController(this);
        
    }
    
    public void addEmployee(Employee emp) {
        final List<Employee> employees = EmployeeStore.getEmployees();
        employees.add(emp);
        notifyView();
        System.out.println("Employee added.");
    }
    
    public Worker newWorker(String name) {
        return EmployeeFactory.createWorker(name);
    }
    
        public Boss newBoss(String name) {
        return EmployeeFactory.createBoss(name);
    }
        
    public void notifyView() {
        v.printResult(EmployeeStore.getEmployees());
    }
    
    public boolean saveEmployeesOnExit() {
        
        try {
            EmployeeStore.saveEmployees();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
    
    public void deleteEmployees() {
        EmployeeStore.deleteEmployees();
        notifyView();
    }
    
}
