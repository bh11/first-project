package mvc_hf3;

import controller.Controller;
import java.util.Scanner;
import model.*;
import view.*;

/**
 *
 * @author Lizie
 */
public class App {
    
    private static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        View v = chooseView();
        Model m = new Model();
        Controller c = new Controller(m, v);
        
        
        v.StartView();
        
               
    }
    
    public static View chooseView() {
        
        do {
            System.out.println("1 - Start console view / 2 - Start swing view");
            if (sc.hasNextInt()) {
                int num = sc.nextInt();
                switch (num) {
                    case 1:
                        return new ConsoleView();
                    case 2:
                        return new SwingView();
                }
            }
        } while (true);
    }
    
}
