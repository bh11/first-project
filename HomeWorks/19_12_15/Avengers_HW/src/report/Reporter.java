/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import stone.StoneType;
import store.Fleet;
import store.Ship;

/**
 *
 * @author czirjak_zoltan
 */
public class Reporter {
    
    private Fleet fleet;

    public Reporter(Fleet fleet) {
        this.fleet = fleet;
    }

    public Reporter() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void generateReports() {
        printCountOfStonesByShip();
    }
    
    private void printCountOfStonesByShip() {
        countOfStonesByShip().entrySet().stream()
                .forEach(i -> {
                    System.out.println("********** Ship: " + i);
                    i.getValue()
                        .forEach((j, k)-> System.out.println("Stone: "+j+" Count: "+k));
                    }
                );
    }
    
    //Hajónként melyik kőből mennyi van?
    private Map<StoneType, Integer> countOfStones(Ship ship) {
        return ship.getHeroes().stream()
                .map(AbstractHero::getStone)
                .collect(Collectors.toMap(
                        k -> k,
                        v -> 1,
                        (v1, v2) -> v1 + v2,
                        HashMap::new
                    )
                );
                
    }
    
    private Map<Ship, Map<StoneType, Integer>> countOfStonesByShip() {
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                        s -> s,
                        this::countOfStones,    // v -> countOfStones(v)
                        (x, y) -> x             //ha ütközik két kulcs, akkor a régi értéket tartjuk meg
                    )
                );
    }
    
    //
    private boolean containsOnlyBornOnEarth(Ship ship) {
        return ship.getHeroes().stream()
                .allMatch(p -> p instanceof BornOnEarth);
    }
    
    private Map<Boolean, List<Ship>> partitioninByContainsOnlyBornOnEarth() {
        return fleet.getShips().stream()
                .collect(Collectors.partitioningBy(this::containsOnlyBornOnEarth));
    }
    
    private int numberOfShipsContainingOnlyOnlyBornOnEarth() {
        return partitioninByContainsOnlyBornOnEarth().get(Boolean.TRUE).size();
    }
    
}
