/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import store.Fleet;

/**
 *
 * @author czirjak_zoltan
 */
public class Saver {

    public static void saveFleet(Fleet fleet) {
        try {
            FileOutputStream fileOut
                    = new FileOutputStream("fleet.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(fleet);
            out.close();
            fileOut.close();
            System.out.printf("Serialized fleet.");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
    
    

}
