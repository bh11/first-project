/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvengersException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author czirjak_zoltan
 */
public class ConsoleReader {
    private static final String STOP = "exit";
    
    private CommandHandler parser = new CommandHandler();
    
    public void read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            
            while (true) {
                String line = br.readLine();
                
                if (line == null || STOP.equals(line)) {
                    break;
                }
                
                parser.process(line);
            }
        
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (InvalidNameAvengersException ex) {
            System.out.println(ex.getMessage());
        }
        
        parser.print();
        parser.report();
        parser.save();
    }
    
}
