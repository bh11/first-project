<%-- 
    Document   : empnames
    Created on : 2020.01.31., 20:52:36
    Author     : Niki
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <table class="result_table">
            <tr><th>Employee names <%
                    if (request.getParameterMap().containsKey("search") && request.getParameter("search") != "") {
                        out.print("cointaining \""+ request.getParameter("search") +"\"");
                    }
                %>
                </th></tr>
            <%
                List<String> names = (List<String>) request.getAttribute("names");

                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }

            %>
        </table>

    </body>
</html>
