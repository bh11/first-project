package hu.braininghub.employees.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.ejb.Singleton;

/**
 *
 * @author Niki
 */
@Singleton
public class EmployeeDao {

    @Resource(lookup = "jdbc/bh11r")
    private DataSource ds;

    @PostConstruct
    public void init() {
        System.out.println("It has been initalized.");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed.");
    }

    public List<String> getNames() throws SQLException {
        List<String> names = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "SELECT *FROM employees limit 10";
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                names.add(rs.getString("first_name"));
            }
        }

        return names;
    }

    public List<String> getFilteredNames(String s) throws SQLException {
        List<String> names = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "SELECT * FROM employees WHERE first_name LIKE \"%" + s + "%\" LIMIT 10";
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                names.add(rs.getString("first_name"));
            }
        }

        return names;
    }

}
