/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.repository;

import hu.braininghub.neptun.repository.entity.Student;
import hu.braininghub.neptun.repository.entity.Subject;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Niki
 */
@Singleton
@LocalBean
public class SubjectDao implements CrudRepository<Subject, Integer>{
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public Iterable<Subject> findAll() {
        return em.createQuery("SELECT s FROM Subject s")
                .getResultList();
    }

    @Override
    public Optional<Subject> findById(Integer id) {
        try {
            Subject s = (Subject) em.createQuery("SELECT s FROM Subject s WHERE s.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(s);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        Subject s = em.find(Subject.class, id);
        if (s != null) {
            em.remove(s);
        }
    }

    @Override
    public void save(Subject entity) {
        em.persist(entity);
    }

    @Override
    public void update(Subject entity) {
        em.merge(entity);
    }

    @Override
    public int count() {
        return em.createQuery("SELECT s FROM Subject s")
                .getResultList().size();
    }
    
}
