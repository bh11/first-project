/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.servlet;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.service.Neptun;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 *
 * @author Niki
 */
@WebServlet (name= "StudentDetailsServlet", urlPatterns = "/details")
public class StudentDetailsServlet extends HttpServlet {
    
    @Inject
    private Neptun neptun;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        int id = Integer.parseInt(req.getParameter("id"));
        
        StudentDto student = neptun.getStudentById(id);
        
        req.setAttribute("student", student);
        
        // id -> service -> dao -> service -> servlet -> jsp
        
        req.getRequestDispatcher("/WEB-INF/studentDetail.jsp").forward(req, resp);
        
    }
}
