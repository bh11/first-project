/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.dto.mapper;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.dto.SubjectDto;
import hu.braininghub.neptun.repository.entity.Student;
import hu.braininghub.neptun.repository.entity.Subject;
import java.util.ArrayList;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Niki
 */
@LocalBean
@Singleton
public class StudentMapper implements Mapper<Student, StudentDto>{
    
    @Inject
    private SubjectMapper subjectMapper;

    @Override
    public Student toEntity(StudentDto dto) {
        
        Student s = new Student();
        
        s.setId(dto.getId());
        s.setName(dto.getName());
        s.setDateOfBirth(dto.getDateOfBirth());
        s.setNeptunCode(dto.getNeptunCode());
        
        s.setSubjects(new ArrayList<>());
        
        for (SubjectDto subjectDto : dto.getSubjects()) {
            s.getSubjects().add(subjectMapper.toEntity(subjectDto));
        }
        
        return s;
    }

    @Override
    public StudentDto toDto(Student entity) {
        StudentDto s = new StudentDto();
        
        s.setId(entity.getId());
        s.setName(entity.getName());
        s.setDateOfBirth(entity.getDateOfBirth());
        s.setNeptunCode(entity.getNeptunCode());
        
        s.setSubjects(new ArrayList<>());
        
        for (Subject subject : entity.getSubjects()) {
            s.getSubjects().add(subjectMapper.toDto(subject));
        }
        
        return s;
    }
    
}
