/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.servlet;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.service.Neptun;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Niki
 */
@WebServlet(name = "StudentServlet", urlPatterns = "/students")
public class StudentServlet extends HttpServlet {

    private static final String NUMBER_OF_REQ = "nrReq";

    @Inject
    private Neptun neptun;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<StudentDto> students = neptun.getStrudents();

        req.setAttribute("students", students);

        req.getRequestDispatcher("/WEB-INF/students.jsp").forward(req, resp);

    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        if (session.getAttribute(NUMBER_OF_REQ) == null) {
            session.setAttribute(NUMBER_OF_REQ, 0);
        } else {
            session.setAttribute(NUMBER_OF_REQ, (int) session.getAttribute(NUMBER_OF_REQ) + 1);
        }
        
        super.service(req, resp);
    }

}
