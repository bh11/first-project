package hu.braininghub.neptun.service;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.dto.mapper.StudentMapper;
import hu.braininghub.neptun.repository.StudentDao;
import hu.braininghub.neptun.repository.entity.Student;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;

/**
 *
 * @author Niki
 */
@Setter
@Singleton
public class Neptun {
    
    @Inject
    private StudentDao dao;
    
    @Inject
    private StudentMapper mapper;
    
    public List<StudentDto> getStrudents() {
        Iterable<Student> students = dao.findAll();
        
        List<StudentDto> dtos = new ArrayList<>();
        
        students.forEach(student -> dtos.add(mapper.toDto(student)));
        
        return dtos;
    }
    
    public StudentDto getStudentById(int id) {
        
        StudentDto studentDto = mapper.toDto(dao.findById(id).orElse(null));
        
        return studentDto;
    }
    
}
