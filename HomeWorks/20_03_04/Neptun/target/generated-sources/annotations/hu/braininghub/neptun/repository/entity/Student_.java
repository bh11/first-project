package hu.braininghub.neptun.repository.entity;

import hu.braininghub.neptun.repository.entity.Subject;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-01T15:02:50")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile ListAttribute<Student, Subject> subjects;
    public static volatile SingularAttribute<Student, String> name;
    public static volatile SingularAttribute<Student, Date> dateOfBirth;
    public static volatile SingularAttribute<Student, Integer> id;
    public static volatile SingularAttribute<Student, String> neptunCode;

}