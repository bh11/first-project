package hu.braininghub.neptun.repository.entity;

import hu.braininghub.neptun.repository.entity.Student;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-01T15:02:50")
@StaticMetamodel(Subject.class)
public class Subject_ { 

    public static volatile SingularAttribute<Subject, Student> student;
    public static volatile SingularAttribute<Subject, String> name;
    public static volatile SingularAttribute<Subject, String> description;
    public static volatile SingularAttribute<Subject, Integer> id;

}