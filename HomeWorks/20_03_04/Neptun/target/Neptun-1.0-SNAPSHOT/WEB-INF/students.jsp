<%-- 
    Document   : students
    Created on : 2020.03.01., 11:24:24
    Author     : Niki
--%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Students: <c:out value="${sessionScope.nrReq}"></c:out></h1>
        <table class="table table-striped table-dark">
            <c:forEach var="s" items="${students}">
                <tr>
                    <td>
                        <c:out value="${s.name}"></c:out>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="/Neptun/details?id=${s.id}" role="button">Link</a>
                        </td>
                    </tr>
            </c:forEach>

        </table>
        <%
            Integer hitsCount = (Integer) application.getAttribute("hitCounter");
            if (hitsCount == null || hitsCount == 0) {
                /* First visit */
                out.println("Welcome to my website!");
                hitsCount = 1;
            } else {
                /* return visit */
                out.println("Visitors: " + hitsCount);
                hitsCount += 1;
            }
            application.setAttribute("hitCounter", hitsCount);
        %>
    </body>
</html>
