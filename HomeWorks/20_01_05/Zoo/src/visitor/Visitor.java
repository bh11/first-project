/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import exception.KidCantFeedAnimalException;

/**
 *
 * @author Niki
 */
public interface Visitor {
    
    void act(AbstractAnimal animal);
    void feed(AbstractAnimal animal) throws KidCantFeedAnimalException;
    
}
