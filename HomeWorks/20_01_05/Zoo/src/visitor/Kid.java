/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import animal.HappinessFactor;
import exception.KidCantFeedAnimalException;

/**
 *
 * @author Niki
 */
public class Kid implements Visitor {
    
    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors() + 2);
        
        animal.setHunger(animal.getHunger() - 1);
        animal.setHappiness(HappinessFactor.nextLevel(animal.getHappiness()));
    }

    @Override
    public void feed(AbstractAnimal animal) throws KidCantFeedAnimalException {
        throw new KidCantFeedAnimalException("Kids can't feed animals. Sorry kids."); //To change body of generated methods, choose Tools | Templates.
    }


}
