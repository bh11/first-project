/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import visitor.Visitor;

/**
 *
 * @author Niki
 */
public class Zoo {
    
    private final List<Cage> cages = new ArrayList<>();
    private final List<Visitor> visitors = new ArrayList<>();
    
    public List<Visitor> getVisitors() {
        return visitors;
    }
    
    public List<Cage> getCages(){
        return cages;
    }    
    
}
