/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import animal.species.Crocodile;
import animal.species.Zebra;
import exception.KidCantFeedAnimalException;
import java.util.logging.Level;
import java.util.logging.Logger;
import visitor.Adult;
import visitor.Kid;
import visitor.Visitor;
import visitor.Worker;

/**
 *
 * @author Niki
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Zoo myZoo = new Zoo();
        
        Worker worker1 = new Worker();
        myZoo.getVisitors().add(worker1);
        
        Cage cage1 = new Cage(); 
        cage1.setWorker(worker1);
        
        System.out.println("New cage: " + cage1.toString());
        
        
        AbstractAnimal zebra1 = new Zebra("Béla", 200);
        AbstractAnimal zebra2 = new Zebra("Gizi", 300);
        AbstractAnimal zebra3 = new Zebra("Jani", 100);
        System.out.println("Add 1st animal: " + cage1.addAnimal(zebra1));
        System.out.println("Add 2nd animal: " + cage1.addAnimal(zebra2));
        System.out.println("Add 3rd animal: " + cage1.addAnimal(zebra3));
        System.out.println("Cage 1: " + cage1.toString());
        
        Cage cage2 = new Cage();
        
        System.out.println("New cage: " + cage2.toString());
        System.out.println("Add 1st animal: " + cage2.addAnimal(zebra3));
        AbstractAnimal croc1 = new Crocodile("Pista", 100);
        System.out.println("Add 2nd animal: " + cage2.addAnimal(croc1));
        
        Visitor visitor1 = new Adult();
        Visitor visitor2 = new Kid();
        
        cage1.visit(visitor1);
        cage1.visit(visitor2);
        System.out.println("Cage 1: " + cage1.toString());
        

        try {
            visitor1.feed(zebra1);
        } catch (KidCantFeedAnimalException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            visitor1.feed(croc1);
        } catch (KidCantFeedAnimalException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Cage 1: " + cage1.toString());
        System.out.println("Cage 2: " + cage2.toString());
        try {
            visitor2.feed(zebra3);
        } catch (KidCantFeedAnimalException ex) {
            System.out.println(ex);
        }
        
        Reporter.report(myZoo.getCages());
    }
    
}
