/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import animal.HappinessFactor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import zoo.Cage;

/**
 *
 * @author Niki
 */
public class Reporter {
    
    public static void report(List<Cage> cages) {
        countSadAndHungryAnimalsInACage(cages);
    }
    
    private static void countSadAndHungryAnimalsInACage(List<Cage> cages) {
        
        List<AbstractAnimal> a;
        int counter = 0;
        
        for (Cage c : cages) {
            a = c.getAnimals();
            for (AbstractAnimal abstractAnimal : a) {
                if (abstractAnimal.getHunger() < 5 && abstractAnimal.getHappiness() != HappinessFactor.HAPPY) {
                    System.out.println(abstractAnimal.toString());
                    counter++;
                    System.out.println(counter);
                }
                System.out.println(counter);
            }
            System.out.println(counter);
        }
        System.out.println(counter);
        
             
                
                
    }
    
}
