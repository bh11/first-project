/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractReptile;
import animal.features.Terrestrial;
import animal.features.Aquatic;

/**
 *
 * @author Niki
 */
public class Crocodile extends AbstractReptile implements Terrestrial, Aquatic {
    
    public Crocodile(String name, int requiedPlace) {
        super(name, requiedPlace);
    }
    
}
