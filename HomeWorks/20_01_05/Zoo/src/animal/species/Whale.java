/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractMammal;
import animal.features.Aquatic;

/**
 *
 * @author Niki
 */
public class Whale extends AbstractMammal implements Aquatic {
    
    public Whale(String name, int requiedPlace) {
        super(name, requiedPlace);
    }
    
}
