/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractMammal;
import animal.features.Terrestrial;
import animal.features.Flying;

/**
 *
 * @author Niki
 */
public class Bat extends AbstractMammal implements Flying, Terrestrial {

    public Bat(String name, int requiedPlace) {
        super(name, requiedPlace);
    }

    @Override
    public void fly() {
        System.out.println("I'm flying!" + toString());
    }
    
}
