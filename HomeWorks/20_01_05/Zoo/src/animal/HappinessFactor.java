/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author Niki
 */
public enum HappinessFactor {
    SAD(0),
    NEUTRAL(1),
    HAPPY(2);

    private final int order;

    private HappinessFactor(int order) {
        this.order = order;
    }

    public static HappinessFactor nextLevel(HappinessFactor currentLevel) {
        Optional<HappinessFactor> nextLevel = Arrays.stream(HappinessFactor.values())
                .filter(f -> f.order == currentLevel.order + 1)
                .findAny();

        return nextLevel.orElse(HAPPY);
    }
}
