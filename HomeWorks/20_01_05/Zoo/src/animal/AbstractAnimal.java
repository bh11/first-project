/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import exception.KidCantFeedAnimalException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import visitor.Adult;
import visitor.Kid;
import visitor.Visitor;
import visitor.Worker;
import zoo.shop.Food;

/**
 *
 * @author Niki
 */
public abstract class AbstractAnimal {

    private final String name;
    private final int requiedPlace;

    private int visitors;
    private LocalDateTime entryCage;
    private double hunger;
    private HappinessFactor happiness = HappinessFactor.SAD;

    public AbstractAnimal(String name, int requiedPlace) {
        this.name = name;
        this.requiedPlace = requiedPlace;
    }

    public int getVisitors() {
        return visitors;
    }

    public String getName() {
        return name;
    }

    public int getRequiedPlace() {
        return requiedPlace;
    }

    public void setVisitors(int visitors) {
        this.visitors = visitors;
    }

    public LocalDateTime getEntryCage() {
        return entryCage;
    }

    public void setEntryCage(LocalDateTime entryCage) {
        this.entryCage = entryCage;
    }

    public double getHunger() {
        return hunger;
    }

    public void setHunger(double hunger) {
        this.hunger = hunger;
    }

    public HappinessFactor getHappiness() {
        return happiness;
    }

    public void setHappiness(HappinessFactor happiness) {
        this.happiness = happiness;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + this.requiedPlace;
        hash = 53 * hash + this.visitors;
        hash = 53 * hash + Objects.hashCode(this.entryCage);
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.hunger) ^ (Double.doubleToLongBits(this.hunger) >>> 32));
        hash = 53 * hash + Objects.hashCode(this.happiness);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractAnimal other = (AbstractAnimal) obj;
        if (this.requiedPlace != other.requiedPlace) {
            return false;
        }
        if (this.visitors != other.visitors) {
            return false;
        }
        if (Double.doubleToLongBits(this.hunger) != Double.doubleToLongBits(other.hunger)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.entryCage, other.entryCage)) {
            return false;
        }
        if (this.happiness != other.happiness) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractAnimal{" + "name=" + name + ", requiedPlace=" + requiedPlace + ", visitors=" + visitors + ", entryCage=" + entryCage + ", hunger=" + hunger + ", happiness=" + happiness + '}';
    }

    public void visit(Visitor visitor) {
        visitor.act(this);
    }

    public void eat(Visitor visitor) throws KidCantFeedAnimalException {

            visitor.feed(this);
   
    }
    
    public abstract void eat(Food food);

}
