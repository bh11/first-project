/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import zoo.shop.Food;

/**
 *
 * @author Niki
 */
public abstract class AbstractReptile extends AbstractAnimal {

    public AbstractReptile(String name, int requiedPlace) {
        super(name, requiedPlace);
    }

    @Override
    public void eat(Food food) {
        setHunger(getHunger() + 1.5);
    }

}
