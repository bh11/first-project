/*
FELADAT:
Celsius -> Fahrenheit átváltó függvény
 */
package hw191106no1;

/**
 *
 * @author Niki
 */
public class Hw191106No1 {

    public static void main(String[] args) {
        // TODO code application logic here
        
        int celsius = 19;
        
        System.out.println(celsius + " °C = " + celsiusToFarenheit(celsius)+ "°F");
    }
    
    static double celsiusToFarenheit(int cels) {
        
        return (cels * 1.8) + 32;
        
    }
    
}
