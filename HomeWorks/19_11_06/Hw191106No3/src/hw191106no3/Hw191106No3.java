/*
adott szám számjegyeinek összege rekurzív módon
 */
package hw191106no3;

/**
 *
 * @author Niki
 */
public class Hw191106No3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int num = 12343;
        System.out.println("A számjegyek összege: " + sumOfDigits(num));
    }

    static int sumOfDigits(int number) {
        if (number == 0) {
            return 0;
        }
        return (number % 10 + sumOfDigits(number / 10));
    }

}
