/*
FELADAT:
írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel 
 valamilyen határok között, majd írjunk egy másik metódust, 
 ami egy n*m-es kétdimenziós tömb legkisebb elemével és annak helyével tér vissza. 
 A visszatérés egy tömb legyen, az alábbi formában [sor, oszlop, érték]
 */
package hw191106no4;

/**
 *
 * @author Niki
 */
public class Hw191106No4 {

    /**
     * @param args the command line arguments
     */
    static int generateRandomNumber(int from, int to) {

        return (int) (Math.random() * (from - to) + to);

    }

    static void fillUpArray(int[][] array, int minValue, int maxValue) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = generateRandomNumber(minValue, maxValue);
            }
        }
    }

    static int[] findMin(int array[][]) {
        /*
        int[0] = min row
        int[1] = min column
        int[2] = min value
         */
        int[] min = new int[3];
        min[2] = Integer.MAX_VALUE;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] < min[2]) {
                    min[2] = array[i][j];
                    min[0] = i;
                    min[1] = j;
                }
            }
        }

        return min;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        int n = 10;
        int m = 10;

        int[][] array2D = new int[n][m];
        fillUpArray(array2D, 0, 100);

        int min[] = findMin(array2D);

        System.out.printf("A legkisebb érték: %d, helye: %d.sor %d.oszlop", min[2], min[1], min[0]);

    }

}
