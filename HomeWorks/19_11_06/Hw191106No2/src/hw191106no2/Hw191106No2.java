/*
FELADAT:
leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel egy 300 elemű tömböt, 
 majd keressük meg h melyik szám került bele a leggyakrabban
 */
package hw191106no2;

/**
 *
 * @author Niki
 */
public class Hw191106No2 {

    public static void main(String[] args) {
        
        int[] array = new int[300];
        
        fillUpArray(array);

        System.out.println("A leggyakoribb elem: " + findMostCommon(array));
        
    }
    
    static int generateRandomNumber(int from, int to) {
        
        return (int)(Math.random()*(from-to)+to);
        
    }
    
    static void fillUpArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = generateRandomNumber(0, 100);
        }
    }
    
    static int[] countNumbers(int[] array) {
        int[] counterArray = new int[100];
        for (int i = 0; i < array.length; i++) {
            counterArray[array[i]]++;
        }
        return counterArray;
    }
    
    static int findMostCommon(int[] array) {
        int[] counterArray = countNumbers(array);
        int max = Integer.MIN_VALUE;
        int maxIndex = 0;
        for (int i = 0; i < counterArray.length; i++) {
            if (counterArray[i] > max) {
                max = counterArray[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    
    }
    
}
