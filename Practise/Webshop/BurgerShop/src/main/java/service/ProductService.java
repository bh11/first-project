/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dto.ProductDto;
import dto.mapper.ProductMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import repository.ProductDao;
import repository.entity.Product;

/**
 *
 * @author Niki
 */
@Setter
@Singleton
public class ProductService {

    @Inject
    private ProductDao productDao;

    @Inject
    private ProductMapper productMapper;

    public List<ProductDto> getAllProducts() {

        Iterable<Product> products = productDao.findAll();

        List<ProductDto> dtos = new ArrayList<>();

        products.forEach(product -> dtos.add(productMapper.toDto(product)));

        return dtos;

    }

}
