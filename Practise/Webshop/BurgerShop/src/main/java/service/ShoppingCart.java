/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;

/**
 *
 * @author Niki
 */
@Stateful
public class ShoppingCart {

    Map<Long, Integer> cart = new HashMap();

    public void addProduct(Long id, int quantity) {
        if (cart.containsKey(id)) {
            quantity += cart.get(id);
        }
        cart.put(id, quantity);
    }

    public void removeProduct(Long id) {
        cart.remove(id);
    }

    public int countOfProducts() {
        int sum = 0;
        for (Integer value : cart.values()) {
            sum += value;
        }
        return sum;
    }
}