/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.mapper;

import dto.ProductDto;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.Product;

/**
 *
 * @author Niki
 */
@LocalBean
@Singleton
public class ProductMapper implements Mapper<Product, ProductDto> {

    @Override
    public Product toEntity(ProductDto dto) {

        Product entity = new Product();
        try {
            BeanUtils.copyProperties(entity, dto);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(ProductMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return entity;
    }

    @Override
    public ProductDto toDto(Product entity) {
        
        ProductDto dto = new ProductDto();
        try {
            BeanUtils.copyProperties(dto, entity);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(ProductMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dto;        
    }

}
