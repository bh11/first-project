/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
public class ProductDto {

    private Long id;
    private String name;
    private int price;
    private String pictureUri;
    private int quantity;

}
