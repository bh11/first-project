/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.ShoppingCart;

/**
 *
 * @author Niki
 */
@WebServlet(name = "ShoppingCartServlet", urlPatterns = {"/shoppingCart/*"})
public class ShoppingCartServlet extends HttpServlet {
    
    @Inject
    private ShoppingCart cart;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        switch(request.getPathInfo().toUpperCase()) {
            case "/ADD": 
                try {
                    cart.addProduct(Long.parseLong(request.getParameter("id")), Integer.parseInt(request.getParameter("qty")));
                } catch (NumberFormatException ex) {
                    System.out.println("ID OR QTY WAS NOT A NUMBER IN SHOPPING CART");
                }
                break;
            case "/REMOVE":
                System.out.println("REMOVE");
                break;
            default:
                System.out.println("LJFDSLKF");
                
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    /**
     * 
     * @param id
     * @return parsed id, or 0 if failed to parse
     */

}
