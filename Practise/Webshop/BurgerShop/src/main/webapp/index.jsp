<%-- 
    Document   : index
    Created on : 2020.04.16., 12:39:17
    Author     : Niki
--%>

<%@page import="service.ShoppingCart"%>
<%@page import="java.util.List"%>
<%@page import="dto.ProductDto"%>
<%
    if (request.getAttribute("Products") == null) {
        request.setAttribute("caller", "index.jsp");
        request.getRequestDispatcher("/products").forward(request, response);
        return;
    }

    List<ProductDto> products = (List<ProductDto>) request.getAttribute("Products");

    ShoppingCart cart = (ShoppingCart) session.getAttribute("ShoppingCart");
    
    if (cart == null) {
        cart = new ShoppingCart();
        session.setAttribute("ShoppingCart", cart);
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="style/style.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <title>Burger Shop</title>
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Burger Shop</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Items in cart: <%= cart.countOfProducts() %></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-lg-3">

                    <div class="list-group mt-4">
                        <a href="#" class="list-group-item">Category 1</a>
                    </div>

                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-9 mt-4">

                    <div class="row">

                        <% for (ProductDto product : products) {%>
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="#"><img class="card-img-top" src="<%= product.getPictureUri()%>" alt=""></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="#"><%= product.getName()%></a>
                                    </h4>
                                    <h5><%= product.getPrice()%> Ft</h5>
                                    <p class="card-text">Some description</p>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary btn-sm" onclick="">Add to cart</button>
                                </div>
                            </div>
                        </div>
                        <% }%>

                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.col-lg-9 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
