/*
SZABÁLYOK
Pálya: min 10, max 20
Bombák: a pálya 10%-a, -1 életpont
Gyógybogyók: a pálya 5%-a +0,5 életpont
Csomag: Random 10-15 között
Kezdő élet: 5
Cél: felvenni az összes csomagot, anélkül hogy meghalnánk.

 */
package game;

import java.util.Scanner;

/**
 *
 * @author Lizie
 */
public class Game {

    static final Scanner SCANNER = new Scanner(System.in);

    static final char BOMB = 'X';
    static final char BERRY = 'O';
    static final char PACKAGE = 'P';
    static final char EMPTY = '\0';
    static final char PLAYER = 'T';

    static final char UP = 'w';
    static final char DOWN = 's';
    static final char RIGHT = 'd';
    static final char LEFT = 'a';

    static double life = 5;

    static int playerX = 0;
    static int playerY = 0;

    static int mapX;
    static int mapY;
    static char[][] map;

    static int getNumber(int from, int to, String message) {
        do {
            System.out.println(message);
            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();
                if (number >= from && number <= to) {
                    return number;
                }
            } else {
                SCANNER.next();
            }
        } while (true);
    }

    static int generateRandomNumber(int from, int to) {
        return (int) (Math.random() * (to - from) + from);
    }

    static void initMap() {
        mapX = getNumber(10, 20, "Pálya X:");
        mapY = getNumber(10, 20, "Pálya Y:");
        map = new char[mapX][mapY];
    }

    static void setPlayer() {
        map[playerX][playerY] = PLAYER;
    }

    static int calculateNumberOfBombs() {
        return (int) (mapX * mapY * 0.1);
    }

    static int calculateNumberOfBerries() {
        return (int) (mapX * mapY * 0.05);
    }

    static int calculateNumberOfPackages() {
        return generateRandomNumber(10, 16);
    }

    static void setSpecialTiles(int number, char character) {
        for (int i = 0; i <= number; i++) {
            int x = generateRandomNumber(0, mapX);
            int y = generateRandomNumber(0, mapY);
            if (map[x][y] == EMPTY) {
                map[x][y] = character;
            } else {
                i--;
            }
        }
    }

    static void printMap() {
        System.out.println("Remaining lifepoints: " + life);
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == EMPTY) {
                    System.out.print(" ");
                } else {
                    System.out.print(map[i][j]);
                }
            }
            System.out.println();
        }
    }
    
    static void step() {
        char c = SCANNER.next().charAt(0);
        switch (c) {
            case DOWN:
                stepDown();
                break;
            case UP:
                stepUp();
                break;
            case LEFT:
                stepLeft();
                break;
            case RIGHT:
                stepRight();
                break;           
       }
    }
    
    static void stepDown() {
        if(playerX < map.length-1) {
            map[playerX][playerY] = EMPTY;
            playerX++;
            handleSpecialTiles();
            map[playerX][playerY] = PLAYER;          
        }        
        printMap();
    }
    static void stepUp() {
        if(playerX > 0) {
            map[playerX][playerY] = EMPTY;
            playerX--;
            handleSpecialTiles();
            map[playerX][playerY] = PLAYER;           
        }        
        printMap();
    }
    static void stepRight() {
        if(playerY < map[0].length-1) {
            map[playerX][playerY] = EMPTY;
            playerY++;
            handleSpecialTiles();
            map[playerX][playerY] = PLAYER;     
            
        }        
        printMap();
    }
    static void stepLeft() {
        if(playerY > 0) {
            map[playerX][playerY] = EMPTY;
            playerY--;
            handleSpecialTiles();
            map[playerX][playerY] = PLAYER;            
        }        
        printMap();
    }
    
    static void handleSpecialTiles() {
        switch (map[playerX][playerY]) {
            case BOMB:
                life--;
                break;
            case BERRY:
                life += 0.5;
                break;
            default:
                System.out.println(map[playerX][playerY]);
        }
    }
    
    static boolean anyPackagesRemaining () {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == PACKAGE) {
                    return true;
                }
            }
        }
        return false;
    
}
    
    static void play() {
        while (life > 0 && anyPackagesRemaining()) {
            step();
        }
        if (life > 0) {
            System.out.println("YOU WON!");
        }
        else  {
            System.out.println("END GAME!");
        }
    }

    public static void main(String[] args) {
        initMap();
        setPlayer();
        setSpecialTiles(calculateNumberOfBombs(), BOMB);
        setSpecialTiles(calculateNumberOfBerries(), BERRY);
        setSpecialTiles(calculateNumberOfPackages(), PACKAGE);
        printMap();
        play();
    }

}
